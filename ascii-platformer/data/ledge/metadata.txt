title 4: Moving Platforms - Science or Sorcery?
size 80 24
start 5 7
message 11 7 0 @ Hey, is that a pyramid down there in the distance? And is it on fire? Can a pyramid catch fire?\ \And I thought I'd seen everything...
platform 9 15 42 15 5
platform 40 5 40 12 4
platform 51 19 64 19 5
message 69 20 0 @ I wonder what sort of magic makes those platforms move.
muncher 65 6
muncher 76 11
goal 70 3 railway
end
