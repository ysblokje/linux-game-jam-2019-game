title 0.5: The drawbridge of exposition
size 80 24
start 1 14
message 12 14 0 @ Ah. Such a perfect night for an adventure.\ \Pharaoh Tutankhascii might be a madman and a blackguard, but he sure picked a pleasant evening for it.
message 37 16 0 @ "Pharaoh Whatanascii?" I hear you ask.\ \Long story short: A wizened old oaf with delusions of grandeur and a veritable army of mummies stormed Queen $'s banquet. The shameless knave then grabbed young princess & and scampered off into the night. It falls to me, the resident hero, to vanquish this horde of hoodlums and bring her home.
message 57 17 0 Signpost .-----------------.\: Adventure ====> : \`-------.-.-------'\        : :        \ \ \Indeed. We're on the right track.
goal 78 17 level1
goal 78 16 level1
goal 78 15 level1
goal 78 14 level1
end
