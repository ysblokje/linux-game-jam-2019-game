# Cursed Pl@tformer 

Cursed Pl@tformer is a simple platformer game written using Curses and SDL2. It can be compiled as a terminal
application using ncurses or as a graphical application with pdcurses. Cursed Pl@tformer was originally
written as an entry in the Linux Game Jam 2019 and is licensed under GPLv3, except for the third-party assets 
which are detailed further in this document.

More information and pre-compiled binaries can be found on the Itch page: [https://samsai.itch.io/cursed-platformer](https://samsai.itch.io/cursed-platformer)  

## Dependencies

This is a rust port of the original game and as such instructions below will be incorrect!

In order to build the game you need SDL2, SDL2-mixer, SDL2-ttf and libncurses. 

## Fetching from gitlab

```sh
git clone --recursive https://gitlab.com/Samsai/linux-game-jam-2019-game.git
```

## Building

The game uses the Meson build system.

To build the game run the following commands:


```sh
cargo build
```



## Credits

* __Samsai__ - Things and stuff
* __Tuubi__  - Odd jobs
* __Ysblokje__ - pdcurses integration etc.


#### Assets:

> __Midnight Tale__ by __Kevin MacLeod__ (incompetech.com)  
> Licensed under Creative Commons: By Attribution 3.0 License  
> http://creativecommons.org/licenses/by/3.0/

CC Attribution-licensed sound effects by freesound.org users:
> jorickhoofd, javapimp

The rest of the included audio was originally released under the CC 0-license.

Font (for pdcurses):
> `Px437_AmstradPC1512-2y.ttf` from [The Ultimate Oldschool PC Font Pack](https://int10h.org/oldschool-pc-fonts/)  
> __(c) 2016 VileR__ / https://int10h.org  
> Licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

