# Cursed Pl@tformer 

Cursed Pl@tformer is a simple platformer game originally written in C using Curses and SDL2 for Linux Game Jam 2019. This is a (mostly faithful) Rust port started by Ysblokje as a language learning project, and Tuubi was later cajoled into contributing some bits.

Cursed Pl@tformer is licensed under GPLv3, except for the third-party assets which are detailed further in this document.

More information and pre-compiled binaries for the original game can be found on the Itch page: [https://samsai.itch.io/cursed-platformer](https://samsai.itch.io/cursed-platformer)  

# The Rust version(s)

This version comes in 2 flavours 

* pure curses based
* pure SDL2 based

On Linux, the curses based version runs in your terminal, using ncurses (via pancurses) for display. The SDL2 version uses our own character based graphics engine, with added support for gamepads and audio via SDL2.

On Windows, pancurses uses PDCurses as its backend, which means that you basically get two SDL2 based games, but only one of them has audio support and nicer controls.

There's no more mixing and matching of platforms, because Ysblokje got lazy towards the end.

## Dependencies

In order to build the game you need a working Rust build environment, including Cargo, on Linux or Windows.

If you don't know how to set it all up, take a look at [rustup](https://rustup.rs/).

## Fetching the source code with git

```
git clone https://codeberg.org/ysblokje/linux-game-jam-2019-game.git
```

## Building and running

To build and run the game, use the following commands:

```
cargo build
```
to compile both versions, or 
```
cargo build --bin cursedplatformercurses
```
for the curses based one, or
```
cargo build --bin cursedplatformersdl2
```
for—you guessed it by now—the SDL2 version.

For an optimized build without debug symbols, add the `--release` switch.

## Running 

Since there are two binaries, `cargo run` will not work. Instead, use the `--bin` switch to run your preferred binary:

```
cargo run --bin cursedplatformercurses
```
or 
```
cargo run --bin cursedplatformersdl2
```

## Command line switches for the game engine

You can start from a level of your choosing by passing the `-m` switch, followed by the name of the folder containing the level data. The default levels can be found under `ascii-platformer/data/`.

For example:
```
cargo run --bin cursedplatformersdl2 --release -- -m ledge
```

For those who wish to create a completely new game, `-g` or `--game` also exists. Use this to point to a path containing the `data` directory of your game. You'll need to copy over the `audio` directory and the ttf font, and you'll need at least one level called `menu`. These aren't easily customizable for now, sorry.

Check out `docs/level-editing.md` for instructions on editing or creating your own levels.


## Credits

* __Ysblokje__ - Most of the work for the new Rust port.
* __Tuubi__  - Odd jobs.
* __Samsai__ - Things and stuff.


#### Assets:

> __Midnight Tale__ by __Kevin MacLeod__ (incompetech.com)  
> Licensed under Creative Commons: By Attribution 3.0 License  
> http://creativecommons.org/licenses/by/3.0/

__CC Attribution-licensed sound effects by freesound.org users:__
> jorickhoofd, javapimp

_The rest of the included audio was originally released under the CC 0-license._

__Font (for pdcurses):__
> `Px437_AmstradPC1512-2y.ttf` from [The Ultimate Oldschool PC Font Pack](https://int10h.org/oldschool-pc-fonts/)  
> __(c) 2016 VileR__ / https://int10h.org  
> Licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
