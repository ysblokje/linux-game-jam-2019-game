extern crate sdl2;

use std::{cell::RefCell, path::Path};

use cursed_core::{game::{GameConfig, game_loop}, io::audio::AudioTraits};
use cursed_core::io::our_sdl2::SDL2;
use sdl2::{pixels::Color, render::Texture};


fn main() -> Result<(), String> {
    let font_path : &str = r#"ascii-platformer/data/Px437_AmstradPC1512-2y.ttf"#;
    let inout = RefCell::new(SDL2::new());
    let mut sdl_context = sdl2::init()?;
    let video_subsys = sdl_context.video()?;
    let event_pump = RefCell::new(sdl_context.event_pump()?);
    let mut colors : Vec<Color> = Vec::new();

    colors.push(Color::RGBA(0,0,0, 255));
    colors.push(Color::RGBA(255,255,255, 255));
    colors.push(Color::RGBA(255,0,0, 255));
    colors.push(Color::RGBA(0,255,0, 255));
    colors.push(Color::RGBA(0,0,255, 255));
    colors.push(Color::RGBA(0,255,255, 255));
    colors.push(Color::RGBA(255,0,255, 255));
    colors.push(Color::RGBA(255,255,0, 255));

    let ttf_context = sdl2::ttf::init().map_err(|e|e.to_string())?;
    let mut font = ttf_context.load_font(font_path, 16)?;

    font.set_kerning(true);
    font.set_hinting(sdl2::ttf::Hinting::Mono);
    let (font_w, font_h) = font.size_of_char('W').unwrap();
    let screen_width : u32 = 80 * font_w;
    let screen_height : u32 = 25 * font_h;

    let window = video_subsys.window("Cursed Platformer",
                                     screen_width, screen_height)
                             .position_centered()
                             .build()
                             .map_err(|e| e.to_string())?;
    let canvas_builder = window.into_canvas()
                               .accelerated()
                               .present_vsync();
    let canvas = RefCell::new(canvas_builder
                                  .build()
                                  .map_err(|e| e.to_string())?);

    canvas.borrow_mut().set_logical_size(screen_width, screen_height).map_err(|e|e.to_string())?;
    let tc = canvas.borrow_mut().texture_creator();
    let mut glyph_set = vec![];

    for color in colors.iter() {
        let mut glyphs : Vec<Texture> = Vec::new();
        for c in 32 as u8 ..=127 as u8 {
            let mut txt : String = String::new();
            txt.push(c as char);

            let surface = font
                .render_char(c as char)
                .blended(*color)
                .unwrap();
            let texture = tc.create_texture_from_surface(surface).map_err(|e|e.to_string())?;

            glyphs.push(texture);
        }
        glyph_set.push(glyphs);
    }


    inout.borrow_mut().init_audio()?;
    inout.borrow_mut().init_input(&mut sdl_context)?;

    let config = GameConfig {
        toggle_music: Box::new(| | inout.borrow_mut().toggle_music()),
        toggle_color: Box::new(| | inout.borrow_mut().toggle_color()),
        fetch_input: Box::new(| | inout.borrow_mut().read(&mut event_pump.borrow_mut())),
        play_sound: Box::new(| s, v | inout.borrow_mut().play_sound(s, v)),
        play_music: Box::new(| f | inout.borrow_mut().play_music(Path::new(f))),
        render: Box::new(| scene | inout.borrow_mut().render(scene, &mut canvas.borrow_mut(),
                                                             &glyph_set, font_h, font_w)),
    };

    game_loop(&config);
    Ok(())
}
