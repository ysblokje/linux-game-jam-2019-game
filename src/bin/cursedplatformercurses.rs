use std::{cell::RefCell, path::Path};
use cursed_core::{game::{GameConfig, game_loop}, io::{audio::AudioTraits, curses::CursesIO, renderer::RenderTraits}};

fn main() -> Result<(), String> {
    let inout = RefCell::new(CursesIO::new());
    inout.borrow_mut().init_renderer(80, 25)?;
    inout.borrow_mut().init_input()?;
    inout.borrow_mut().init_audio()?;

    let config = GameConfig {
        toggle_music: Box::new(| | inout.borrow_mut().toggle_music()),
        toggle_color: Box::new(| | inout.borrow_mut().toggle_color()),
        fetch_input: Box::new(| | inout.borrow_mut().read()),
        play_sound: Box::new(| s, v | inout.borrow_mut().play_sound(s, v)),
        play_music: Box::new(| f | inout.borrow_mut().play_music(Path::new(f))),
        render: Box::new(| scene | inout.borrow_mut().render(scene)),
    };

    game_loop(&config);

    Ok(())
}
