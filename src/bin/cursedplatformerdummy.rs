use cursed_core::game::{GameConfig, game_loop};


fn main() -> Result<(), String> {

    let config = GameConfig {
        toggle_music: Box::new(| | ()),
        toggle_color: Box::new(| | ()),
        fetch_input: Box::new(| | vec![]),
        play_sound: Box::new(| _s, _v | ()),
        play_music: Box::new(| _f | ()),
        render: Box::new(| _scene | ()),
    };

    game_loop(&config);

    Ok(())
}
