use std::str::SplitAsciiWhitespace;

use crate::{audio, level};

use super::entity::{BaseEntity, EntityType, EntityState, OptionEntityPtr, PFReturnType, read_base};
use crate::lib::read_x_y;

#[derive(Default)]
struct Button {
    base : BaseEntity,
    door : (usize, usize),
}

impl EntityType for Button {
    fn draw(&self, _level : &mut level::Level) {
        todo!()
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut level::Level) -> PFReturnType {
        let x = self.base.x as i32;
        let y = self.base.y as i32;

        let px = level.player.base.x as i32;
        let py = level.player.base.y as i32;

        if x == px && y == py {
            audio::play_sound(audio::SoundTypes::PRESSUREPLATE);
            let (x, y) = self.door;
            level.map[x as usize][y as usize] = ' ';
            self.base.disabled = true;
        }
        PFReturnType::NothingToReport
    }
}

pub fn create_button(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut button = read_base::<Button>(iter);
    button.door = read_x_y::<usize>(iter, 0);
    button.base.width = 1;
    button.base.sprite = '_';
    button.base.color = 5;
    button.base.state = EntityState::Idle;
    button.base.disabled = false;
    button.base.hidden = false;
    Some(button)
}
