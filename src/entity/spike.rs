use std::str::SplitAsciiWhitespace;

use crate::{audio, level::Level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
pub struct Spike {
    base : BaseEntity,
}

impl EntityType for Spike {
    fn draw(&self, _level : &mut Level) {
        todo!()
    }

    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType {
        if let EntityState::Garbage = self.base.get_state() {
            return PFReturnType::NothingToReport;
        }

        let x = self.base.x as i32;
        let mut y = self.base.y as i32;

        let px = level.player.base.x as i32;
        let py = level.player.base.y as i32;

        match self.base.state {
            EntityState::Idle => {
                if x == px && y < py {
                    while y < py {
                        if level.map[y as usize][x as usize]!='\0' {
                            break;
                        }
                        y+=1;

                        if y == py {
                            self.base.set_state(EntityState::Airborne);
                        }
                    }
                }
            },
            EntityState::Airborne => {
                if x == px && y == py - 1 {
                    level.player.base.state = EntityState::Dead;
                    audio::play_sound(audio::SoundTypes::DEATH)
                }
                self.base.process_physics(delta, level);
            },
            EntityState::Garbage => {
                self.base.set_sprite('*');
                self.base.set_state(EntityState::Garbage);
                audio::play_sound(audio::SoundTypes::RUBBLE);
            },
            _ => (),
        }
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}

pub fn create_spike(iter : &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut spike = read_base::<Spike>(iter);
    spike.base.width = 1;
    spike.base.x_vel = 0.0;
    spike.base.y_vel = 0.0;
    spike.base.sprite = 'V';
    spike.base.color = 7;
    spike.base.state = EntityState::Idle;
    spike.base.disabled = false;
    spike.base.hidden = false;
    Some(spike)
}
