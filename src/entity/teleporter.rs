use std::str::SplitAsciiWhitespace;

use crate::{audio, level::Level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};
use crate::lib::read_x_y;

#[derive(Default)]
pub struct Teleport {
    base : BaseEntity,
    dest_x : i32,
    dest_y : i32,
}
pub fn create_teleport(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut teleport = read_base::<Teleport>(iter);
    let (x, y) = read_x_y(iter, 0);
    teleport.dest_x = x;
    teleport.dest_y = y;
    teleport.base.width = 1;
    teleport.base.x_vel = 0.0;
    teleport.base.sprite = '|';
    teleport.base.state = EntityState::Idle;
    teleport.base.disabled = false;
    teleport.base.hidden = false;
    Some(teleport)
}

impl EntityType for Teleport {
    fn draw(&self, _level : &mut Level) {}

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        if self.base.x.round() == level.player.base.x.round() && self.base.y.round() == level.player.base.x.round() {
            let tx = self.dest_x;
            let ty = self.dest_y;

            level.player.base.x = tx as f32;
            level.player.base.y = ty as f32;
            audio::play_sound(audio::SoundTypes::TELEPORT);
        }
        PFReturnType::NothingToReport
    }
}
