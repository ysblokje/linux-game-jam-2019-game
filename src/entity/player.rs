use super::entity::BaseEntity;

pub struct Player {
    pub base : BaseEntity,
}

impl Default for Player {
    fn default() -> Self {
        let mut retval = Self{base : Default::default()};
        retval.base.sprite = '@';
        retval.base.color = 2;
        retval
    }
}
