pub use std::collections::HashMap;
use std::{default::Default, str::SplitAsciiWhitespace};
use crate::{audio, level::{self, Level}, lib::read_x_y};

pub enum PFReturnType {
    NothingToReport = 0,
    TheLevelChanged = 1,
    PauzeTiming = 2
}

pub type Color = u8;
//pub struct Color {}


#[derive(PartialEq)]
pub enum EntityState {
    Walking = 0,
    Airborne,
    Idle,
    Dead,
    Garbage,
}

impl Default for EntityState {
    fn default() ->  Self { EntityState::Walking }
}

pub trait EntityType {
    fn draw(&self, level : &mut Level);
    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType;
    fn get_mut_base(&mut self) -> &mut BaseEntity;
    fn get_base(&self) -> &BaseEntity;
}

pub type EntityPtr = Box<dyn EntityType>;
pub type OptionEntityPtr = Option<EntityPtr>;
pub type Entities = Vec<EntityPtr>;

pub type CreateEntityFuncPtr = fn(config: &mut SplitAsciiWhitespace) -> Option<EntityPtr>;
type EntityCreatorMap = HashMap<String, CreateEntityFuncPtr>;

pub type EntityBrain = fn (delta: f32, level : &mut Level) -> PFReturnType;

#[derive(Default)]
pub struct EntityFactory {
    pub entity_creators : EntityCreatorMap,
}

impl EntityFactory {
    pub fn create_factory() -> EntityFactory {
        let ecm : EntityFactory = EntityFactory { entity_creators : EntityCreatorMap::new() };
        ecm
    }
    pub fn register_create_func_ptr(&mut self, id: &str, cefp: CreateEntityFuncPtr) -> bool {
        if let Some(_func) = self.entity_creators.get(id) {
            return false;
        } else {
            self.entity_creators.insert(id.to_string(), cefp);
        }
        true
    }
    pub fn create_entity(&self, id: &str, config: &mut SplitAsciiWhitespace) -> Option<EntityPtr> {
        if let Some(func) = self.entity_creators.get(id) {
            println!("Creating entity : {}", id);
            return func(config);
        }
        None
    }
}

pub struct BaseEntity {
    pub width : i32,
    pub height : i32,
    pub hidden: bool,
    pub disabled : bool,
    pub sprite : char,
    pub state : EntityState,
    pub color : Color,

    pub x : f32,
    pub y : f32,
    pub x_vel : f32,
    pub y_vel : f32,
}

impl Default for BaseEntity {
    fn default() -> Self {
        Self{
            width : 1,
            height : 1,
            hidden: false,
            disabled : false,
            sprite : ' ',
            state : EntityState::default(),
            color : 0,

            x : 0.0,
            y : 0.0,
            x_vel : 0.0,
            y_vel : 0.0
        }
    }
}

pub trait GetBaseProperties {
    fn get_mut_ref_x(&mut self) -> &mut f32;
    fn get_mut_ref_y(&mut self) -> &mut f32;
    fn set_x_y(&mut self, x_y : (f32, f32));
}

impl BaseEntity {
    pub fn set_state(&mut self, state : EntityState) {
        self.state = state;
    }
    pub fn get_state(&self) -> &EntityState {
        &self.state
    }

    pub fn get_width(&self) -> i32 {
        self.width
    }

    pub fn get_height(&self) -> i32 {
        self.height
    }

    pub fn is_disabled(&self) -> bool {
        self.disabled
    }

    pub fn sprite(&self) -> char {
        self.sprite
    }
    pub fn set_sprite(&mut self, new_sprite : char) {
        self.sprite = new_sprite;
    }

    pub fn state(&self) -> &EntityState {
        &self.state
    }

    pub fn color(&self) -> Color {
        self.color
    }

    pub fn get_x(&self) -> f32 {
        self.x
    }

    pub fn get_y(&self) -> f32 {
        self.y
    }

    pub fn get_x_y(&self) -> (f32, f32) {
        (self.x, self.y)
    }

    pub fn get_x_y_rounded(&self) -> (i32, i32) {
        (self.x as i32, self.y as i32)
    }
    pub fn set_x(&mut self, x : f32) {
        self.x = x;
    }

    pub fn set_y(&mut self, y : f32) {
        self.y = y;
    }

    pub fn get_vel_x(&self) -> f32 {
        self.x_vel
    }
    pub fn get_vel_y(&self) -> f32 {
        self.y_vel
    }
    pub fn set_vel_x(&mut self, x_vel : f32) {
        self.x_vel = x_vel;
    }
    pub fn set_vel_y(&mut self, y_vel : f32) {
        self.y_vel = y_vel;
    }
    pub fn set_x_y(&mut self, (x, y) : (f32, f32)) {
        self.x = x;
        self.y = y;
    }

    pub fn stop(&mut self) {
        self.x_vel = 0.0;
        self.y_vel = 0.0;
    }

    pub fn process_physics(&mut self, delta : f32, level : &mut level::Level) {
        let old_x = self.get_x() as i32;
        let old_y = self.get_y() as i32;

        let map = level.get_map();

        if self.get_vel_x() < 0.0 && !is_passable(map[old_y as usize][(old_x - 1) as usize]) {
            self.set_x(old_x as f32);
        }

        if self.get_vel_x() > 0.0 && !is_passable(map[old_y as usize][(old_x + 1) as usize]) {
            self.set_x(old_x as f32);
        }

        self.set_y(self.get_y() + self.get_vel_y() * delta);
        self.set_x(self.get_y() + self.get_vel_x() * delta);

        let mut new_x = self.get_x() as i32;

        if !is_passable(map[old_y as usize][new_x as usize]) {
            self.set_x(old_x as f32);
            new_x = old_x;
        }

        let mut new_y = self.get_y();

        if !is_passable(map[new_y as usize][new_x as usize]) {
            self.set_y(old_y as f32);
            new_y = old_y as f32;
        }

        if is_passable(map[(new_y + 1.0) as usize][new_x as usize]) {
            self.set_vel_y(self.get_vel_y() + 4.0 * delta);
        }

        let below = map[(new_y + 1.0) as usize][new_x as usize];
        if below == '^' {
            if self.sprite() != 'V' {
                audio::play_sound(audio::SoundTypes::DEATH);
            }
            self.set_state(EntityState::Dead);
            self.set_x(new_x as f32);
            self.set_y(new_y as f32);
        } else if (!is_passable(below) || below == '"' || below == 'T') && self.get_vel_y() >= 0.0 {
            self.set_state(EntityState::Walking);
            self.set_vel_y(0.0);
            self.set_y(new_y as f32);
        } else {
            self.set_vel_y(self.get_vel_y() + 12.0 * delta);
            self.set_state(EntityState::Airborne);
        }
    }
}



pub fn read_base<T: Default + EntityType>(iter : &mut SplitAsciiWhitespace) -> Box<T> {
    let mut retval = Box::new(T::default());
    retval.get_mut_base().set_x_y(read_x_y(iter, 0.0));
    retval
}

//TODO
pub fn is_passable(_tile : char) -> bool {
    todo!();
}
