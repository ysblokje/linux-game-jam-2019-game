use std::str::SplitAsciiWhitespace;

use crate::{audio, level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
pub struct Bat {
    base : BaseEntity,
    sprite_timer : f32,
}

impl EntityType for Bat {
    fn draw(&self, _level : &mut level::Level) {
        todo!()
    }

    fn process_delta(&mut self, delta : f32, level : &mut level::Level) -> PFReturnType {
        self.sprite_timer += delta;

        if self.sprite_timer > 0.2 {
            if self.base.sprite == '^' {
                self.base.sprite = 'v';
            } else {
                self.base.sprite = '^';
            }
            self.sprite_timer = 0.0;
        }

        let x = self.base.x;
        let y = self.base.y;

        let px = level.player.base.x;
        let py = level.player.base.y;

        // Kill.round() on touch
        if x.round() == px.round() && y.round() == py.round() {
            if level.player.base.state != EntityState::Dead {
                level.player.base.state = EntityState::Dead;
                audio::play_sound(audio::SoundTypes::DEATH);
            }
        }

        let dx = (x - px).abs();
        let dy = (y - py).abs();


        let dist = (dx.powi(2) + dy.powi(2)).sqrt();

        if dist < 5.0 {
            self.base.x_vel = px - x;
            self.base.y_vel = py - y;
        }

        let map = &mut level.map;

        let block_ahead : char;

        if (self).base.x_vel < 0.0 {
            block_ahead = map[y as usize][x as usize - 1];
        } else {
            block_ahead = map[y as usize][x  as usize + 1];
        }
        if block_ahead == '#' || block_ahead == '^' {
            self.base.x_vel = -self.base.x_vel;
        }

        self.base.process_physics(delta, level);
        self.base.y_vel = 0.0;
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}


pub fn create_bat(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut bat = read_base::<Bat>(iter);
    bat.base.width = 1;
    bat.base.x_vel = -4.0;
    bat.base.sprite = '^';
    bat.base.color = 5;
    bat.base.state = EntityState::Walking;
    bat.base.disabled = false;
    bat.base.hidden = false;
    Some(bat)
}
