use std::str::SplitAsciiWhitespace;

use crate::{audio, level::Level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
struct Trampoline{
    base : BaseEntity,
}

pub fn create_trampoline(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut trampoline = read_base::<Trampoline>(iter);
    trampoline.base.width = match iter.next() {
        Some(val) => val.parse::<i32>().unwrap_or(1),
        _ => 1
    };
    trampoline.base.x_vel = 0.0;
    trampoline.base.sprite = 'Z';
    trampoline.base.state = EntityState::Idle;
    trampoline.base.disabled = false;
    trampoline.base.hidden = false;
    Some(trampoline)
}

impl EntityType for Trampoline {

    fn draw(&self, _level : &mut Level) {
        todo!()
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        let (x, y) = self.base.get_x_y();
        let (px,py) = level.player.base.get_x_y();

        if px.round() >= x.round() &&
            px.round() < (x.round() + self.base.width as f32) &&
            py > y - 0.5 && py < y {
                if level.player.base.y_vel > 4.0 {
                    level.player.base.y_vel *= -0.9;
                }
                self.base.sprite = 'z';
                let vel = level.player.base.y_vel * -1.0;
                if vel > 6.0 {
                    //setSoundVolume(SND_TRAMPOLINE, (int) (((vel - 6) / 6) * 128) );
                    audio::play_sound(audio::SoundTypes::TRAMPOLINE);
                }
            } else {
                self.base.sprite = 'Z';
            }
        PFReturnType::NothingToReport
    }
}
