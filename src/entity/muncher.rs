use std::str::SplitAsciiWhitespace;

use super::entity::{self, BaseEntity, EntityPtr, EntityState, PFReturnType};
use super::entity::read_base;
use crate::level::Level;

#[derive(Default)]
pub struct Muncher {
    base : BaseEntity,
    sprite_timer : f32
}

/*
 * Selfs just have a starting position
 * self X Y
 */
pub fn create_muncher(config: &mut SplitAsciiWhitespace) -> Option<EntityPtr> {
    let mut retval = read_base::<Muncher>(config);
    retval.base.x_vel = -4.0;
    retval.base.sprite = '<';
    retval.base.state = EntityState::Walking;
    retval.base.disabled = false;
    retval.base.hidden = false;
    retval.base.color = 5;
    retval.sprite_timer = 0.0;
    Some(retval)
}


impl entity::EntityType for Muncher {
    fn draw(&self, _level : &mut Level) {

    }

    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType {
        self.sprite_timer += delta;

        if self.sprite_timer > 0.5 {
            if  self.base.sprite == '-' {
                self.base.sprite = '<';
            } else {
                self.base.sprite = '-';
            }
            self.sprite_timer = 0.0;
        }

        if self.base.x_vel < 0.0 && self.base.sprite != '-' {
            self.base.sprite = '>';
        } else if self.base.sprite != '-'  {
            self.base.sprite = '<';
        }

        let xy = self.base.get_x_y_rounded();

        match level.get_player().base.state {
            EntityState::Dead => {},
            _ => {
                let pxy = level.get_player().base.get_x_y_rounded();

                // Kill player on touch
                if xy.eq(&pxy) {
                    level.get_player().base.state = EntityState::Dead;
                    //playSound(SND_DEATH);
                }
            },
        }

        let map = level.get_map();

        let mut block_ahead : char;
        let x = xy.0 as usize;
        let y = xy.1 as usize;


        if self.base.x_vel < 0.0 {
            block_ahead = map[y][x-1];// >data[y][x - 1];
        } else {
            block_ahead = map[y][x + 1];
        }
        if block_ahead == '#' || block_ahead == '^' {
            self.base.x_vel *= -1.0;
        }

        if self.base.x_vel < 0.0 {
            block_ahead = map[y + 1][x - 1];
        } else  {
            block_ahead = map[y + 1][x + 1];
        }

        if block_ahead == ' ' || block_ahead == '^' {
            self.base.x_vel *= -1.0;
        }

        //TODO
        self.get_mut_base().process_physics(delta, level);
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}
