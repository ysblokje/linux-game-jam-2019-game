use std::str::SplitAsciiWhitespace;

use crate::level;
use super::entity::{BaseEntity, EntityType, EntityState, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
struct Message {
    base : BaseEntity,
    who : String,
    msg : String,
    read : bool
}

impl EntityType for Message {
    fn draw(&self, _level : &mut level::Level) {
        todo!()
    }

    fn process_delta(&mut self, _delta : f32, level : &mut level::Level) -> PFReturnType {
        let x = self.base.x;
        let y = self.base.y;

        let px = level.player.base.x;
        let py = level.player.base.y;

        if  !self.read && x == px && y == py {
            level.show_message(&self.who, &self.msg);
            self.read = true;
            self.base.disabled = true;
            self.base.sprite = '!';
            return PFReturnType::PauzeTiming;
        }
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}

pub fn create_message(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut message = read_base::<Message>(iter);
    if  let Some(_val) = iter.next() {
        // Some readable value, what does that mean?
        if let Some(val) = iter.next() {
            message.who = val.to_string();
            let mut s = String::new();
            while let Some(val) = iter.next() {
                s.push_str(val);
            }
            message.msg = s;
        }
    }
    message.base.width = 1;
    message.base.sprite = '?';
    message.base.color = 7;
    message.base.state = EntityState::Idle;
    message.base.disabled = false;
    message.base.hidden = false;
    message.read = false;
    Some(message)
}
