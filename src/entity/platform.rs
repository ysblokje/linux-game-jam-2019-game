use std::str::SplitAsciiWhitespace;

use crate::level;
use super::entity::{BaseEntity, EntityType, EntityState, OptionEntityPtr, PFReturnType, read_base};
use crate::lib::read_x_y;

#[derive(Default)]
struct Platform {
    base : BaseEntity,
    direction : char,
    sx : f32,
    sy : f32,
    tx : f32,
    ty : f32,
}

pub fn create_platform(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut platform = read_base::<Platform>(iter);
    platform.base.width = match iter.next() {
        Some(val) => val.parse::<i32>().unwrap_or(1),
        _ => 1
    };
    let dest_pos = read_x_y::<f32>(iter, 0.0);

    platform.sx = platform.base.x;
    platform.sy = platform.base.y;
    platform.tx = dest_pos.0;
    platform.ty = dest_pos.1;

    if platform.sx == platform.tx {
        platform.direction = 'V';
    } else {
        platform.direction = 'H';
    }


    platform.base.x_vel = 0.0;
    platform.base.y_vel = 0.0;
    platform.base.sprite = 'T';
    platform.base.state = EntityState::Idle;
    platform.base.disabled = false;
    platform.base.hidden = false;
    Some(platform)
}

impl EntityType for Platform {
    fn draw(&self, _level : &mut level::Level) {
        todo!()
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, delta : f32, level : &mut level::Level) -> PFReturnType {
        let x = self.base.x;
        let y = self.base.y;

        let px = level.player.base.x;
        let py = level.player.base.y;

        self.base.x_vel = 0.0;
        self.base.y_vel = 0.0;

        if self.direction == 'V' {
            let dir = self.ty - y;

            if dir < 0.0 {
                self.base.y_vel = -3.0;
            } else {
                self.base.y_vel = 3.0;
            }
        } else {
            let dir = self.tx - x;

            if dir < 0.0 {
                self.base.x_vel = -3.0;
            } else {
                self.base.x_vel = 3.0;
            }
        }

        if x == self.tx && y == self.ty {
            self.tx = self.sx;
            self.ty = self.sy;

            self.sx = x;
            self.sy = y;

            self.base.x = self.sx;
            self.base.y = self.sy;
        }

        self.base.x += self.base.x_vel * delta;
        self.base.y += self.base.y_vel * delta;

        if px >= x && px < x + self.base.width as f32 && py == y - 1.0 {
            if level.player.base.state != EntityState::Dead {
                level.player.base.state = EntityState::Walking;
            }
            level.player.base.y_vel = 0.0;
            level.player.base.y = self.base.y - 1.0;
            level.player.base.x += self.base.x_vel * delta;
        }

        PFReturnType::NothingToReport
    }
}
