use std::{io::{BufRead, BufReader}, iter::repeat, path::{Path, PathBuf}, str::SplitAsciiWhitespace};
use std::fs::File;
use crate::{entity::entity::Color, lib::read_x_y};
use crate::{entity::{entity::{Entities, EntityFactory}, player::Player}, map::{GameMap, ColorMap}};

#[derive(Default)]
pub struct MetaData {
    pub title : String,
    pub width : u16,
    pub height : u16,
    pub start_x : u16,
    pub start_y : u16,
    // Are these really needed?
    pub goal_x : u16,
    pub goal_y : u16,
}

pub struct Level<'a> {
    pub player : Player,
    pub map : GameMap,
    pub color_map : ColorMap,
    pub name : String,
    pub entities : Entities,
    pub metadata : MetaData,
    pub starting_path : String,
    pub entity_factory : &'a EntityFactory,
}

impl<'a> Level<'a> {
    pub fn new(gamepath : &str, ef : &'a EntityFactory) -> Self {
        Self {
            player : Default::default(),
            map : GameMap::default(),
            color_map : ColorMap::default(),
            name : String::default(),
            entities : Entities::default(),
            metadata : MetaData::default(),
            starting_path : String::from(gamepath),
            entity_factory : ef,
        }
    }
    pub fn get_player(&mut self) -> &mut Player {
        &mut self.player
    }
    pub fn get_map(&mut self) -> &mut GameMap {
        &mut self.map
    }
    pub fn show_message(&mut self, _who : &String, _test : &String) {
        self.player.base.stop();
        todo!()
    }
    pub fn switch_level(&mut self, level : &str) {
        Self::load_level(self, level)
    }
    fn load_level(&mut self, name : &str) {
        self.name = String::from(name);
        self.player = Player::default();
        let base_path = Path::new(self.starting_path.as_str()).join("data");
        let map_path = base_path.clone().join(name).join("map.txt");
        let colormap_path = base_path.clone().join(name).join("colormap.txt");
        let metadata_path = base_path.clone().join(name).join("metadata.txt");

        self.entities.clear();
        self.map.clear();
        self.color_map.clear();

        Self::read_metadata(self, &metadata_path);
        Self::load_map(self, &map_path);
        Self::load_colormap(self, &colormap_path);
    }

    fn parse_metaline(&mut self, command : &str, iter: &mut SplitAsciiWhitespace) {
        match command {
            "title" => {
                while let Some(val) = iter.next() {
                    self.metadata.title += " ";
                    self.metadata.title += val;
                }
            },
            "size" => {
                let (width, height) = read_x_y(iter, 0);
                self.metadata.width = width;
                self.metadata.height = height;
            },
            "start" => {
                let (x, y) = read_x_y::<f32>(iter, 0.0);
                self.player.base.x = x;
                self.player.base.y = y;
            }
            _ => {
                match self.entity_factory.create_entity(command, iter) {
                    Some(entity) => {
                        self.entities.push(entity);
                    },
                    _ => {}
                }
            }
        }
    }

    fn read_metadata(&mut self, file_path : &PathBuf) -> bool {
        if let Ok(file) = File::open(file_path) {
            let mut reader = BufReader::new(file);
            let mut line = String::new();
            while let Ok(len) = reader.read_line(&mut line) {
                if len == 0 {
                    return true;
                }
                println!("read metaline : {}", line);
                let mut iter = line.split_ascii_whitespace();
                match iter.next() {
                    Some(command) => Self::parse_metaline(self, command, &mut iter),
                    _ => ()
                };
                line.clear();
            }
            return true
        }
        false
    }

    fn load_map(&mut self, file_path : &PathBuf) {
        if let Ok(file) = File::open(file_path) {
            let mut reader = BufReader::new(file);
            let mut line = String::new();
            while let Ok(len) = reader.read_line(&mut line) {
                if len == 0 {
                    break;
                }
                let mut row : Vec<char> = line.chars().collect();
                let padding = self.metadata.width as i16 - row.len() as i16;
                if padding > 0 {
                    let mut pads = [' ' as char].repeat(padding as usize);
                    row.append(&mut pads);
                }
                self.map.push(row);
                //self.map.push(line.chars().collect::<Vec<char>>());
                line.clear();
            }
        }
    }

    fn load_colormap(&mut self, file_path : &PathBuf) {
        if let Ok(file) = File::open(file_path) {
            let mut reader = BufReader::new(file);
            let mut line = String::new();
            while let Ok(len) = reader.read_line(&mut line) {
                if len == 0 {
                    break;
                }
                let row : Vec<Color> =
                    line.chars().take_while(|c| (*c as Color > ' ' as Color))
                                .map(|c| c as Color - ' ' as Color)
                                .chain(repeat(0 as Color).take(self.metadata.width as usize - (len-1)))
                                .collect();
                self.color_map.push(row);
                line.clear();
            }
        }
    }
}

/*

struct Level* loadLevel(char* name, struct Entity* player) {
    struct Level* level = malloc(sizeof(struct Level));

    memset(level, 0, sizeof(struct Level));

    strcpy(level->name, name);

    char path[80] = "data/";
    strcat(path, name);

    char metadata_path[80];
    strcpy(metadata_path, path);
    strcat(metadata_path, "/metadata.txt");

    level->enemy_count = 0;
    for (int i = 0; i < MAX_ENTITIES; i++) {
        level->enemies[i] = NULL;
    }

    struct Metadata* metadata = readMetadata(level, metadata_path);

    level->metadata = metadata;
    level->map = loadMap(metadata, path);
    level->player = player;
    level->player->level = level;

    player->x_vel = 0;
    player->y_vel = 0;

    player->x = level->metadata->start_x;
    player->y = level->metadata->start_y;

    return level;
}

void addEntity(struct Level* level, struct Entity* entity) {
    for (int i = 0; i < MAX_ENTITIES; i++) {
        if (level->enemies[i] == NULL) {
            entity->level = level;
            level->enemies[i] = entity;
            level->enemy_count++;

            return;
        }

        if (level->enemies[i]->state == GARBAGE) {
            destroyEntity(level->enemies[i]);
            level->enemies[i] = entity;

            return;
        }
    }
}

void destroyLevel(struct Level* level) {
    free(level->metadata);
    destroyMap(level->map);

    for (int i = 0; i < MAX_ENTITIES; i++) {
        if (level->enemies[i] != NULL) {
            destroyEntity(level->enemies[i]);
        }
    }

    free(level);
}

void switchLevel(char* name, struct Entity* player) {
    char buf[80];
    strcpy(buf, name);

    struct Level* level = getCurrentLevel();

    destroyLevel(level);

    setCurrentLevel(loadLevel(buf, player));
}

void processMetaDataLine(char const * line, struct Metadata* metadata, struct Level *level) {
    char const *command = line;
    const int command_length = strlen(line);

        if (memcmp(command, "title", 5) == 0) {
            command += 6;
            strncpy(metadata->title, command, sizeof(metadata->title));
        } else if (memcmp(command, "size", 4) == 0) {
            int width;
            int height;
            command += 5;
            sscanf(command, "%d %d", &width, &height);
            metadata->width = width;
            metadata->height = height;
        } else if (memcmp(command, "start", 5) == 0) {
            int x;
            int y;
            command += 6;
            sscanf(command, "%d %d", &x, &y);
            metadata->start_x = x;
            metadata->start_y = y;
        } else if (memcmp(command, "goal", 4) == 0) {
            int x;
            int y;
            command += 5;
            char next_level[80];

            sscanf(command, "%d %d %s", &x, &y, next_level);

            struct Entity* goal = createGoal(level, x, y, next_level);
            addEntity(level, goal);
        } else if (memcmp(command, "muncher", 7) == 0) {
            int x;
            int y;
            command += 8;

            sscanf(command, "%d %d", &x, &y);

            struct Entity* muncher = createMuncher(level, x, y);
            addEntity(level, muncher);
        } else if (memcmp(command, "spike", 5) == 0) {
            int x;
            int y;
            command += 6;
            sscanf(command, "%d %d", &x, &y);

            struct Entity* spike = createSpike(level, x, y);
            addEntity(level, spike);
        } else if (memcmp(command, "bat", 3) == 0) {
            int x;
            int y;
            command += 4;
            sscanf(command, "%d %d", &x, &y);

            struct Entity* bat = createBat(level, x, y);
            addEntity(level, bat);
        } else if (memcmp(command, "message", 7) == 0) {
            int x, y;
            int readable;
            char who[40];
            char msg[341];
            command += 8;

            int bytesread = sscanf(command, "%d %d %d %s %341[^\n]\n", &x, &y, &readable, who, msg);

            struct Entity* message = createMessage(level, x, y, who, msg, readable);
            addEntity(level, message);
        } else if (memcmp(command, "button", 6) == 0) {
            int x, y;
            int tx, ty;
            command += 7;

            sscanf(command, "%d %d %d %d", &x, &y, &tx, &ty);

            struct Entity* button = createButton(level, x, y, tx, ty);
            addEntity(level, button);
        } else if (memcmp(command, "teleport", 8) == 0) {
            int x, y;
            int tx, ty;
            command += 9;
            sscanf(command, "%d %d %d %d", &x, &y, &tx, &ty);

            struct Entity* teleport = createTeleport(level, x, y, tx, ty);
            addEntity(level, teleport);
        } else if (memcmp(command, "platform", 8) == 0) {
            int sx, sy;
            int tx, ty;
            int width;
            command += 9;

            sscanf(command, "%d %d %d %d %d", &sx, &sy, &tx, &ty, &width);

            struct Entity* platform = createPlatform(level, sx, sy, tx, ty, width);
            addEntity(level, platform);
        } else if (memcmp(command, "trampoline", 10) == 0) {
            int sx, sy, width;
            command += 11;
            sscanf(command, "%d %d %d", &sx, &sy, &width);

            struct Entity* trampoline = createTrampoline(level, sx, sy, width);
            addEntity(level, trampoline);
        } else if (memcmp(command, "continue", 8) == 0) {
            int x, y;
            command += 9;

            sscanf(command, "%d %d", &x, &y);

            struct Entity* cont = createContinue(level, x, y);
            addEntity(level, cont);
        } else if (strcmp(command, "//") == 0) {
//            char buf[250];
//            fgets(buf, 250, fp);
        }

}

/**
 * Shows a modal message window that can be closed with any key.
 * char*    who     Optional name of quotee, or an empty string.
 * char*    text    Message text. Can contain several lines of max
 *                  54 chars delimited with "\n".
 * return   int     1 if player should stop moving
 **/
int showMessage(char *who, char *text) {
    int stop = 0;
    WINDOW *frame;
    WINDOW *win;
    char *token;
    int i = 0;
    char str[400];
    strncpy(str, text, 400);
    int maxy = getmaxy(mainwin);
    int maxx = getmaxx(mainwin);
    int wh = MIN((float)maxy * 0.8, 16);
    int ww = MIN((float)maxx * 0.8, 56);
    int sh, sw;

    frame = subwin(mainwin, wh, ww, (float) (maxy - wh) / 2, (float) (maxx - ww) / 2);
    werase(frame);

    sh = wh - 2;
    // Smaller horizontal margins if space is tight
    if (ww > 40) {
        sw = ww - 4;
    } else {
        sw = ww - 2;
    }
    win = derwin(frame, sh, sw, (wh - sh) / 2, (ww - sw) / 2);

    if (strlen(who) > 0) {
        wattron(win, COLOR_PAIR(7));
        mvwprintw(win, 0, 0, "%s says:", who);
        wattroff(win, COLOR_PAIR(7));
    }
    i = 2;

    mvwaddstr(win, sh - 1, 0, "[ ");
    wattron(win, COLOR_PAIR(3));
    waddstr(win, "(A)");
    wattroff(win, COLOR_PAIR(3));
    waddstr(win, " / Space ]");

    box(frame, 0, 0);

    int lines = wrap(str, sw - 1);

    token = strtok(str, "\n");
    while (token != NULL) {
        mvwaddstr(win, sh - 1, sw - 2, ">>");
        if (i > sh - 3) {
            wrefresh(mainwin);
            wrefresh(frame);
            wrefresh(win);
            if (pauseForAction()) {
                stop = 1;
            }
            for (int j = 2; j < i; j++) {
                wmove(win, j, 0);
                wclrtoeol(win);
            }
            i = 2;
        }
        mvwaddnstr(win, i, 0, token, sw);
        i += 1;
        token = strtok(NULL, "\n");
    }
    wmove(win, sh - 1, sw - 2);
    wclrtoeol(win);

    wrefresh(mainwin);
    wrefresh(frame);
    wrefresh(win);
    if (pauseForAction()) {
        stop = 1;
    }
    delwin(win);
    delwin(frame);
    return stop;
}
*/
