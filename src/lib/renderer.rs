
use crate::level::Level;

pub trait RenderTraits {
    fn render(&mut self, _level : &Level);
}
