use std::str::{FromStr, SplitAsciiWhitespace};
pub mod renderer;

/*
This is nice place to define some "base" things.
*/

pub fn read_x_y<T: Copy + FromStr>(iter : &mut SplitAsciiWhitespace, alternative : T) ->
 (T, T) {
    let x : T = match iter.next() {
        Some(val) => val.parse::<T>().unwrap_or(alternative),
        _ => alternative
    };

    let y : T = match iter.next() {
        Some(val) => val.parse::<T>().unwrap_or(alternative),
        _ => alternative
    };

    (x,y)
}
