use std::path::Path;

use super::{Audio, AudioImplementation, SoundTypes};

extern crate pancurses;


pub struct Beep;

impl Audio for Beep {
    fn init_audio(&mut self) {}
    fn play_sound(&mut self, _sound : &SoundTypes, _ : i32) {
        pancurses::beep();
    }
    fn play_music(&mut self, _music_file : &Path) {}
    fn toggle_music(&mut self) {}
}

impl Beep {
    pub fn new() -> AudioImplementation {
        Box::new(Beep)
    }
}
