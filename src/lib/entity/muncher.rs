use std::str::SplitAsciiWhitespace;

use entity::process_physics;

use super::entity::{self, BaseEntity, EntityPtr, EntityState, PFReturnType};
use super::entity::read_base;
use crate::{level::Level, io::audio::SoundTypes};

#[derive(Default)]
pub struct Muncher {
    base : BaseEntity,
    sprite_timer : f32
}

/*
 * Selfs just have a starting position
 * self X Y
 */
pub fn create_muncher(config: &mut SplitAsciiWhitespace) -> Option<EntityPtr> {
    let mut retval = read_base::<Muncher>(config);
    retval.base.x_vel = -4.0;
    retval.base.sprite = '<';
    retval.base.state = EntityState::Walking;
    retval.base.disabled = false;
    retval.base.hidden = false;
    retval.base.color = 2;
    retval.sprite_timer = 0.0;
    Some(retval)
}


impl entity::EntityType for Muncher {
    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType {
        self.sprite_timer += delta;

        if self.sprite_timer > 0.5 {
            if  self.base.sprite == '-' {
                self.base.sprite = '<';
            } else {
                self.base.sprite = '-';
            }
            self.sprite_timer = 0.0;
        }

        if self.base.x_vel < 0.0 && self.base.sprite != '-' {
            self.base.sprite = '>';
        } else if self.base.sprite != '-'  {
            self.base.sprite = '<';
        }

        let xy = self.base.get_x_y_rounded();

        if level.get_player().base.state != EntityState::Dead {
            // Kill player on touch
            if level.get_player().base.get_x_y_rounded() == self.base.get_x_y_rounded() {
                level.get_player_mut().kill();
                level.play_sound(SoundTypes::DEATH, None);
            }
        }

        let map = level.get_map();
        let vel_x = self.base.get_vel_x();
        let ahead_x : f32;
        if vel_x > 0.0 {
            ahead_x = (self.base.get_x() + 1.0).floor();
        } else {
            ahead_x = (self.base.get_x() - 1.0).ceil();
        }
        let ahead = map[xy.1 as usize][ahead_x as usize];
        let below = map[(xy.1 + 1) as usize][ahead_x as usize];
        // Turn around if the way ahead is blocked or there's no floor
        if !entity::is_passable(ahead) || !entity::is_surface(below) {
            self.base.set_vel_x(vel_x * -1.0);
        }
        let vel_x = self.base.get_vel_x();

        process_physics(&mut self.base, delta, level.get_map_mut());
        if self.base.get_state() != EntityState::Dead {
            self.base.set_vel_x(vel_x); 
        }

        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}
