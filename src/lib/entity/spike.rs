use std::str::SplitAsciiWhitespace;

use crate::{io::audio::SoundTypes, level::Level};
use super::entity::{self, BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, process_physics, read_base};

#[derive(Default)]
pub struct Spike {
    base : BaseEntity,
}

impl EntityType for Spike {

    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType {
        if let EntityState::Dead = self.base.get_state() {
            return PFReturnType::NothingToReport;
        }

        let x = self.base.get_x_i32();
        let mut y = self.base.get_y_i32();

        let px = level.get_player().base.get_x_i32();
        let py = level.get_player().base.get_y_i32();

        match self.base.state {
            EntityState::Idle => {
                if x == px && y < py {
                    while y < py {
                        if !entity::is_passable(level.get_map()[y as usize][x as usize]) {
                            break;
                        }

                        y+=1;
                        if y == py {
                            self.base.set_state(EntityState::Airborne);
                        }
                    }
                }
            },
            EntityState::Airborne => {
                if x == px && y == py - 1 {
                    let player = level.get_player_mut();
                    if player.base.get_state() != EntityState::Dead {
                        player.kill();
                        level.play_sound(SoundTypes::DEATH, None);
                    }
                }
                process_physics(&mut self.base, delta, level.get_map_mut());
                // If the spike has hit something, turn it into rubble
                if self.base.get_state() == EntityState::Dead {
                    self.base.set_sprite('*');
                    level.play_sound(SoundTypes::RUBBLE, None);
                }
            },
            _ => (),
        }
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}

pub fn create_spike(iter : &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut spike = read_base::<Spike>(iter);
    spike.base.width = 1;
    spike.base.x_vel = 0.0;
    spike.base.y_vel = 0.0;
    spike.base.sprite = 'V';
    spike.base.color = 7;
    spike.base.state = EntityState::Idle;
    spike.base.disabled = false;
    spike.base.hidden = false;
    Some(spike)
}
