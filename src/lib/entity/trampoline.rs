use std::str::SplitAsciiWhitespace;

use crate::{io::audio::SoundTypes, level::Level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
struct Trampoline{
    base : BaseEntity,
}

pub fn create_trampoline(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut trampoline = read_base::<Trampoline>(iter);
    trampoline.base.width = match iter.next() {
        Some(val) => val.parse::<i32>().unwrap_or(1),
        _ => 1
    };
    trampoline.base.x_vel = 0.0;
    trampoline.base.sprite = 'Z';
    trampoline.base.color = 1;
    trampoline.base.state = EntityState::Idle;
    trampoline.base.disabled = false;
    trampoline.base.hidden = false;
    Some(trampoline)
}

impl EntityType for Trampoline {

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        let x_left = self.base.get_x_i32();
        let y = self.base.get_y().ceil() as i32;
        let x_right = x_left + self.base.width;
        let px = level.get_player().base.get_x_i32();
        let py = level.get_player().base.get_y().floor() as i32;

        if px >= x_left && px < x_right && py == y - 1 {
            let vel = level.get_player().base.y_vel;
            if vel > 6.0 {
                let volume = (((vel - 6.0) / 6.0) * 128.0).ceil() as i32;
                level.play_sound(SoundTypes::TRAMPOLINE, Some(volume));
            }
            if vel > 2.0 {
                level.get_player_mut().base.y_vel *= -0.9;
                self.base.sprite = 'z';
            }
        } else {
            self.base.sprite = 'Z';
        }
        PFReturnType::NothingToReport
    }
}
