use std::str::SplitAsciiWhitespace;

use crate::{io::audio::SoundTypes, level::Level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};
use crate::read_x_y;

#[derive(Default)]
pub struct Teleport {
    base : BaseEntity,
    dest_x : i32,
    dest_y : i32,
}
pub fn create_teleport(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut teleport = read_base::<Teleport>(iter);
    let (x, y) = read_x_y(iter, 0);
    teleport.dest_x = x;
    teleport.dest_y = y;
    teleport.base.width = 1;
    teleport.base.x_vel = 0.0;
    teleport.base.sprite = '|';
    teleport.base.color = 1;
    teleport.base.state = EntityState::Idle;
    teleport.base.disabled = false;
    teleport.base.hidden = false;
    Some(teleport)
}

impl EntityType for Teleport {

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        if self.base.get_x_y_rounded() == level.get_player().base.get_x_y_rounded() {
            let tx = self.dest_x;
            let ty = self.dest_y;

            level.get_player_mut().base.set_x(tx as f32);
            level.get_player_mut().base.set_y(ty as f32);
            level.play_sound(SoundTypes::TELEPORT, None);
        }
        PFReturnType::NothingToReport
    }
}
