pub mod entity;
pub mod player;
pub mod muncher;
pub mod trampoline;
pub mod goal;
pub mod cont;
pub mod spike;
pub mod teleporter;
pub mod bat;
pub mod platform;
pub mod button;
pub mod message;

pub fn register_assemblyline(factory : &mut entity::EntityFactory) {
    factory.register_create_func_ptr("muncher", muncher::create_muncher);
    factory.register_create_func_ptr("trampoline", trampoline::create_trampoline);
    factory.register_create_func_ptr("goal", goal::create_goal);
    factory.register_create_func_ptr("cont", cont::create_cont);
    factory.register_create_func_ptr("spike", spike::create_spike);
    factory.register_create_func_ptr("teleport", teleporter::create_teleport);
    factory.register_create_func_ptr("bat", bat::create_bat);
    factory.register_create_func_ptr("platform", platform::create_platform);
    factory.register_create_func_ptr("button", button::create_button);
    factory.register_create_func_ptr("message", message::create_message);
    factory.register_create_func_ptr("continue", cont::create_cont);
    //TODO the rest of them?
}
