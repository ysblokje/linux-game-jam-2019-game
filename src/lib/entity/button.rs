use std::str::SplitAsciiWhitespace;

use crate::{io::audio::SoundTypes, level::Level};

use super::entity::{BaseEntity, EntityType, EntityState, OptionEntityPtr, PFReturnType, read_base};
use crate::read_x_y;

#[derive(Default)]
struct Button {
    base : BaseEntity,
    door : (usize, usize),
}

impl EntityType for Button {

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        if !self.base.disabled {
            let x = self.base.get_x_i32();
            let y = self.base.get_y_i32();

            let px = level.get_player().base.get_x_i32();
            let py = level.get_player().base.get_y_i32();

            if x == px && y == py {
                level.play_sound(SoundTypes::PRESSUREPLATE, None);
                let (x, y) = self.door;
                level.get_map_src_mut()[y as usize][x as usize] = ' ';
                self.base.disabled = true;
            }
        }
        PFReturnType::NothingToReport
    }
}

pub fn create_button(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut button = read_base::<Button>(iter);
    button.door = read_x_y::<usize>(iter, 0);
    button.base.width = 1;
    button.base.sprite = '_';
    button.base.color = 5;
    button.base.state = EntityState::Idle;
    button.base.disabled = false;
    button.base.hidden = false;
    Some(button)
}
