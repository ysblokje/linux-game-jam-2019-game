pub use std::collections::HashMap;
use std::{default::Default, str::SplitAsciiWhitespace};

use crate::{level::Level, map::GameMap};
use crate::read_x_y;

pub enum PFReturnType {
    NothingToReport = 0,
    TheLevelChanged = 1,
    PauseTiming = 2,
    ToggleMusic = 3,
    ToggleColor = 4,
    QuitGame,
}

pub type Color = u8;
//pub struct Color {}


#[derive(PartialEq, Clone)]
pub enum EntityState {
    Walking = 0,
    Airborne,
    Idle,
    Dead
}

impl Default for EntityState {
    fn default() ->  Self { EntityState::Walking }
}

pub trait EntityType {
    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType;
    fn get_mut_base(&mut self) -> &mut BaseEntity;
    fn get_base(&self) -> &BaseEntity;
}

pub type EntityPtr = Box<dyn EntityType>;
pub type OptionEntityPtr = Option<EntityPtr>;
pub type Entities = Vec<EntityPtr>;

pub type CreateEntityFuncPtr = fn(config: &mut SplitAsciiWhitespace) -> Option<EntityPtr>;
type EntityCreatorMap = HashMap<String, CreateEntityFuncPtr>;

pub type EntityBrain = fn (delta: f32, level : &mut Level) -> PFReturnType;

#[derive(Default)]
pub struct EntityFactory {
    pub entity_creators : EntityCreatorMap,
}

impl EntityFactory {
    pub fn create_factory() -> EntityFactory {
        let ecm : EntityFactory = EntityFactory { entity_creators : EntityCreatorMap::new() };
        ecm
    }
    pub fn register_create_func_ptr(&mut self, id: &str, cefp: CreateEntityFuncPtr) -> bool {
        if let Some(_func) = self.entity_creators.get(id) {
            return false;
        } else {
            self.entity_creators.insert(id.to_string(), cefp);
        }
        true
    }
    pub fn create_entity(&self, id: &str, config: &mut SplitAsciiWhitespace) -> Option<EntityPtr> {
        if let Some(func) = self.entity_creators.get(id) {
            return func(config);
        }
        None
    }
}

pub struct BaseEntity {
    pub width : i32,
    pub height : i32,
    pub hidden: bool,
    pub disabled : bool,
    pub sprite : char,
    pub state : EntityState,
    pub color : Color,

    x : f32,
    y : f32,
    pub x_vel : f32,
    pub y_vel : f32,
}

impl Default for BaseEntity {
    fn default() -> Self {
        Self{
            width : 1,
            height : 1,
            hidden: false,
            disabled : false,
            sprite : ' ',
            state : EntityState::default(),
            color : 0,

            x : 0.0,
            y : 0.0,
            x_vel : 0.0,
            y_vel : 0.0
        }
    }
}

pub trait GetBaseProperties {
    fn get_mut_ref_x(&mut self) -> &mut f32;
    fn get_mut_ref_y(&mut self) -> &mut f32;
    fn set_x_y(&mut self, x_y : (f32, f32));
}

impl BaseEntity {
    pub fn set_state(&mut self, state : EntityState) {
        self.state = state;
    }
    pub fn get_state(&self) -> EntityState {
        self.state.clone()
    }

    pub fn get_width(&self) -> i32 {
        self.width
    }

    pub fn get_height(&self) -> i32 {
        self.height
    }

    pub fn is_disabled(&self) -> bool {
        self.disabled
    }

    pub fn get_sprite(&self) -> char {
        self.sprite
    }
    pub fn set_sprite(&mut self, new_sprite : char) {
        self.sprite = new_sprite;
    }

    pub fn get_color(&self) -> Color {
        self.color
    }

    pub fn set_color(&mut self, color : Color) {
        self.color = color;
    }

    pub fn get_x(&self) -> f32 {
        self.x
    }

    pub fn get_y(&self) -> f32 {
        self.y
    }

    pub fn get_x_y(&self) -> (f32, f32) {
        (self.x, self.y)
    }

    pub fn get_x_y_rounded(&self) -> (i32, i32) {
        (self.x.round() as i32, self.y.round() as i32)
    }
    pub fn set_x(&mut self, x : f32) {
        self.x = x;
    }

    pub fn set_y(&mut self, y : f32) {
        self.y = y;
    }

    pub fn get_vel_x(&self) -> f32 {
        self.x_vel
    }
    pub fn get_vel_y(&self) -> f32 {
        self.y_vel
    }
    pub fn set_vel_x(&mut self, x_vel : f32) {
        self.x_vel = x_vel;
    }
    pub fn set_vel_y(&mut self, y_vel : f32) {
        self.y_vel = y_vel;
    }
    pub fn set_x_y(&mut self, (x, y) : (f32, f32)) {
        self.x = x;
        self.y = y;
    }

    pub fn stop(&mut self) {
        self.x_vel = 0.0;
        self.y_vel = 0.0;
    }

    pub fn get_x_i32(&self) -> i32 {
        self.x.round() as i32
    }
    pub fn get_y_i32(&self) -> i32 {
        self.y.round() as i32
    }
    pub fn get_x_mut(&mut self) -> &mut f32 {
        return &mut self.x;
    }
    pub fn get_y_mut(&mut self) -> &mut f32 {
        return &mut self.y;
    }

}


pub fn process_physics(entity : &mut BaseEntity, delta : f32, map : &mut GameMap) {
    let terminal_velocity = 13.0;
    let old_x = entity.get_x();
    let old_y = entity.get_y();

    let vel_x = entity.get_vel_x();
    let mut vel_y = entity.get_vel_y();

    let mut state: EntityState = entity.get_state().clone();

    // can we be where we want to go?
    let mut new_x = old_x + vel_x * delta;
    let mut new_y = old_y + vel_y * delta;

    let mut dir = 0.0;

    if vel_x > 0.0 { dir = 1.0 } else if vel_x < 0.0 { dir = -1.0 };
    if !is_passable(map[old_y.round() as usize][(old_x.round() + dir) as usize]) {
        new_x = old_x.round();
    }

    if vel_y > 0.0 { dir = 1.0 } else if vel_y < 0.0 { dir = -1.0 };
    let tile = map[(old_y.round() + dir) as usize][new_x.round() as usize];
    if (vel_y >= 0.0 && is_surface(tile)) || (vel_y < 0.0 && !is_passable(tile)) {
        if vel_y >= 0.0 && (tile == '^' || entity.get_sprite() == 'V') {
            new_y = old_y.round();
            state = EntityState::Dead;
            vel_y = 0.0;
        } else if vel_y >= 0.0 {
            new_y = old_y.round();
            state = EntityState::Walking;
            vel_y = 0.0;
        } else {
            new_y = old_y.round();
        }
    }

    // Apply gravity
    vel_y = (vel_y + 16.0 * delta).min(terminal_velocity);
    if !is_surface(map[(new_y.round() + 1.0) as usize][new_x.round() as usize]) {
        state = EntityState::Airborne;
    }

    entity.set_vel_y(vel_y);
    entity.set_vel_x(vel_x);

    entity.set_y(new_y);
    entity.set_x(new_x);

    if entity.get_state() != EntityState::Dead {
        entity.set_state(state);
    }
}

pub fn read_base<T: Default + EntityType>(iter : &mut SplitAsciiWhitespace) -> Box<T> {
    let mut retval = Box::new(T::default());
    retval.get_mut_base().set_x_y(read_x_y(iter, 0.0));
    retval
}

pub fn is_passable(tile : char) -> bool {
    !['#', '^', ']', '['].contains(&tile)
}

pub fn is_surface(tile : char) -> bool {
    ['#', '^', ']', '[', '"', 'T'].contains(&tile)
}
