use std::{fs, str::SplitAsciiWhitespace};
use crate::level::Level;
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
struct Goal {
    base : BaseEntity,
    next_level : String,
}

pub fn create_goal(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut goal = read_base::<Goal>(iter);
    goal.next_level = match iter.next() {
        Some(val) => val.to_string(),
        None => "menu".to_string()
    };
    goal.base.width = 1;
    goal.base.sprite = ' ';
    goal.base.color = 0;
    goal.base.state = EntityState::Idle;
    goal.base.disabled = false;
    goal.base.hidden = true;
    Some(goal)
}

impl EntityType for Goal {

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        let x = self.base.get_x_i32();
        let y = self.base.get_y_i32();

        let px = level.get_player().base.get_x_i32();
        let py = level.get_player().base.get_y_i32();

        if x == px && y == py {
            let _save = fs::write("autosave.sav", self.next_level.as_str());
            level.next_level = self.next_level.clone();
            return PFReturnType::TheLevelChanged;
        }
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

}
