use std::str::SplitAsciiWhitespace;
use crate::level::Level;

use super::entity::{BaseEntity, EntityType, OptionEntityPtr, PFReturnType, read_base, EntityState};

#[derive(Default)]
struct Message {
    base : BaseEntity,
    who : String,
    msg : String,
}

impl EntityType for Message {

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        if !self.base.disabled {
            let x = self.base.get_x_i32();
            let y = self.base.get_y_i32();

            let px = level.get_player().base.get_x_i32();
            let py = level.get_player().base.get_y_i32();

            if  x == px && y == py {
                self.show_message(level);
                self.base.disabled = true;
                self.base.sprite = '!';
                return PFReturnType::PauseTiming;
            }
        }
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}

impl Message {

    fn show_message(&mut self, level : &mut Level) {
        level.show_message(&self.who, &self.msg);
    }
}

pub fn create_message(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut message = read_base::<Message>(iter);
    if  let Some(_val) = iter.next() {
        // Some readable value, what does that mean?
        if let Some(val) = iter.next() {
            message.who = val.to_string();
            let mut s = String::new();
            while let Some(val) = iter.next() {
                s.push_str(val);
                s.push_str(" ");
            }
            message.msg = s.clone();
        }
    }
    message.base.width = 1;
    message.base.sprite = '?';
    message.base.color = 7;
    message.base.state = EntityState::Idle;
    message.base.disabled = false;
    message.base.hidden = false;
    Some(message)
}
