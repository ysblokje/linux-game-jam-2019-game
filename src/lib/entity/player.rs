
use crate::io::input::Input;

use super::entity::{BaseEntity, EntityState};

pub struct Player {
    pub base : BaseEntity,
}

impl Default for Player {
    fn default() -> Self {
        let mut retval = Self{base : Default::default()};
        retval.base.sprite = '@';
        retval.base.color = 1;
        retval
    }
}


impl Player {
    pub fn stop(&mut self) {
        self.base.x_vel = 0.0;
        self.base.y_vel = 0.0;
    }

    pub fn kill(&mut self) {
        self.base.set_state(EntityState::Dead);
        self.base.set_sprite('X');
        self.base.set_color(2);
        self.base.set_vel_x(0.0);
        self.base.set_vel_y(0.0);
    }

    pub fn process_input(&mut self, keys : &Vec<Input>) -> EntityState {
        if self.base.get_state() != EntityState::Dead {
            for event in keys.into_iter() {
                match event {
                    Input::STOPMOVINGRIGHT => {
                        if self.base.x_vel > 0.0 {
                            self.base.x_vel = 0.0;
                        }
                    }
                    Input::STOPMOVINGLEFT => {
                        if self.base.x_vel < 0.0 {
                            self.base.x_vel = 0.0;
                        }
                    }
                    Input::LEFT => {
                        if self.base.x_vel < 0.0 {
                            self.base.x_vel = 0.0;
                        } else {
                            self.base.x_vel = -8.0;
                        }
                    }
                    Input::RIGHT => {
                        if self.base.x_vel > 0.0 {
                            self.base.x_vel = 0.0;
                        } else {
                            self.base.x_vel = 8.0;
                        }
                    }
                    Input::UP | Input::ACTION => {
                        if self.base.get_state() == EntityState::Walking {
                            self.base.y_vel = -9.0;
                            self.base.set_state(EntityState::Airborne);
                        }
                    }
                    _ => (),
                }
            }
        }
        self.base.get_state().clone()
    }
}
