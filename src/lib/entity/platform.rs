use std::str::SplitAsciiWhitespace;

use crate::level::Level;
use super::entity::{BaseEntity, EntityType, EntityState, OptionEntityPtr, PFReturnType, read_base};
use crate::read_x_y;

#[derive(Default)]
struct Platform {
    base : BaseEntity,
    reversing : bool,
    sx : f32,
    sy : f32,
    tx : f32,
    ty : f32,
}

pub fn create_platform(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut platform = read_base::<Platform>(iter);
    let dest_pos = read_x_y::<f32>(iter, 0.0);
    platform.base.width = match iter.next() {
        Some(val) => val.parse::<i32>().unwrap_or(1),
        _ => 4
    };
    platform.reversing = false;

    platform.sx = platform.base.get_x();
    platform.sy = platform.base.get_y();
    platform.tx = dest_pos.0;
    platform.ty = dest_pos.1;

    if platform.sx == platform.tx {
        platform.base.set_vel_x(0.0);
        let vel = if platform.sy < platform.ty { 3.0 } else { -3.0 };
        platform.base.set_vel_y(vel);
    } else {
        let vel = if platform.sx < platform.tx { 3.0 } else { -3.0 };
        platform.base.set_vel_x(vel);
        platform.base.set_vel_y(0.0);
    }


    platform.base.sprite = 'T';
    platform.base.color = 1;
    platform.base.state = EntityState::Idle;
    platform.base.disabled = false;
    platform.base.hidden = false;
    Some(platform)
}

impl EntityType for Platform {

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType {
        let x = self.base.get_x();
        let y = self.base.get_y();

        let px = level.get_player().base.get_x();
        let py = level.get_player().base.get_y();

        // Are we there yet?
        let cond = if self.reversing {
            x.floor() == self.sx && y.round() == self.sy
        } else {
            x.floor() == self.tx && y.round() == self.ty
        };
        if cond {
            // Flip direction
            self.reversing = !self.reversing;
            self.base.set_vel_y(-self.base.get_vel_y());
            self.base.set_vel_x(-self.base.get_vel_x());
        }

        self.base.set_x(x + self.base.x_vel * delta);
        self.base.set_y(y + self.base.y_vel * delta);

        let x = self.base.get_x().floor();
        let y = self.base.get_y().round();
        let player = level.get_player_mut();
        if player.base.get_vel_y() > self.base.get_vel_y() - 1.0 && (x..(x + self.base.width as f32).floor()).contains(&px) && py.floor() == y - 1.0 {
            if player.base.get_state() != EntityState::Dead {
                player.base.set_state(EntityState::Walking);
            }
            player.base.set_vel_y(0.0);
            player.base.set_y(self.base.get_y().round() - 1.0 + self.base.get_vel_y() * delta);
            player.base.set_x(player.base.get_x() + self.base.get_vel_x() * delta);
            player.base.set_state(EntityState::Walking);
        }

        PFReturnType::NothingToReport
    }
}
