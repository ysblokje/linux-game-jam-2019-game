use std::{fs::File, io::{BufRead, BufReader}, str::SplitAsciiWhitespace};

use crate::level::Level;

use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, read_base};

#[derive(Default)]
struct Continue {
    base : BaseEntity,
}

impl EntityType for Continue {

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }

    fn process_delta(&mut self, _delta : f32, level : &mut Level) -> PFReturnType {
        let x = self.base.get_x_i32();
        let y = self.base.get_y_i32();

        let px = level.get_player().base.get_x_i32();
        let py = level.get_player().base.get_y_i32();

        if x == px && y == py {
            if let Ok(savefile) = File::open("autosave.sav") {
                let mut reader = BufReader::new(savefile);
                let mut line = String::new();
                if let Ok(len) = reader.read_line(&mut line) {
                    if len > 0 {
                        level.next_level = line;
                    }
                    return PFReturnType::TheLevelChanged;
                }
            }
        }
        PFReturnType::NothingToReport
    }
}

pub fn create_cont(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut cont = read_base::<Continue>(iter);
    cont.base.width = 1;
    cont.base.sprite = ' ';
    cont.base.color = 0;
    cont.base.state = EntityState::Idle;
    cont.base.disabled = false;
    cont.base.hidden = true;
    Some(cont)
}
