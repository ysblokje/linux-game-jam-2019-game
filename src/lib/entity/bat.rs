use std::str::SplitAsciiWhitespace;

use crate::{io::audio::SoundTypes, level::Level};
use super::entity::{BaseEntity, EntityState, EntityType, OptionEntityPtr, PFReturnType, process_physics, read_base, is_passable};

#[derive(Default)]
pub struct Bat {
    base : BaseEntity,
    sprite_timer : f32,
}

impl EntityType for Bat {

    fn process_delta(&mut self, delta : f32, level : &mut Level) -> PFReturnType {
        self.sprite_timer += delta;

        if self.sprite_timer > 0.2 {
            if self.base.sprite == '^' {
                self.base.sprite = 'v';
            } else {
                self.base.sprite = '^';
            }
            self.sprite_timer -= 0.2;
        }

        let x = self.base.get_x();
        let y = self.base.get_y();

        let px = level.get_player().base.get_x();
        let py = level.get_player().base.get_y();

        // Kill on touch
        if level.get_player().base.get_state() != EntityState::Dead {
            if self.base.get_x_y_rounded() == level.get_player().base.get_x_y_rounded() {
                level.get_player_mut().kill();
                level.play_sound(SoundTypes::DEATH, None);
            }
        }

        let dx = (x - px).abs();
        let dy = (y - py).abs();


        let dist = (dx.powi(2) + dy.powi(2)).sqrt();

        if dist < 5.0 && level.get_player_mut().base.get_state() != EntityState::Dead {
            self.base.x_vel = px - x;
            self.base.y_vel = py - y;
        } else {
            self.base.set_vel_x(self.base.get_vel_x().signum() * 4.0);
            self.base.set_vel_y(0.0);
        }

        let check_x = if self.base.get_vel_x() > 0.0 { x.floor() + 1.0 } else { x.ceil() - 1.0 };
        let ahead = level.get_tile(y as usize, (check_x) as usize);
        if !is_passable(ahead) || ahead == '^' {
            self.base.set_vel_x(-self.base.x_vel);
        }

        process_physics(&mut self.base, delta, level.get_map_mut());
        self.base.y_vel = 0.0;
        PFReturnType::NothingToReport
    }

    fn get_mut_base(&mut self) -> &mut BaseEntity {
        &mut self.base
    }

    fn get_base(&self) -> &BaseEntity {
        &self.base
    }
}


pub fn create_bat(iter: &mut SplitAsciiWhitespace) -> OptionEntityPtr {
    let mut bat = read_base::<Bat>(iter);
    bat.base.width = 1;
    bat.base.x_vel = -4.0;
    bat.base.sprite = '^';
    bat.base.color = 5;
    bat.base.state = EntityState::Walking;
    bat.base.disabled = false;
    bat.base.hidden = false;
    Some(bat)
}
