use std::{io::{BufRead, BufReader}, iter::repeat, path::{Path, PathBuf}, str::SplitAsciiWhitespace};
use std::fs::File;

use crate::{entity::{entity::process_physics, player::Player}, io::{audio::SoundTypes, input}, map::{ColorMap, GameMap}};
use crate::entity::entity::PFReturnType;
use crate::entity::entity::EntityState;
use crate::entity::entity::EntityFactory;
use crate::entity::entity::Entities;
use crate::entity::entity::Color;
use crate::entity::entity::BaseEntity;

use crate::read_x_y;

#[derive(Default)]
pub struct MetaData {
    pub title : String,
    pub width : u16,
    pub height : u16,
    pub start_x : u16,
    pub start_y : u16,
    // Are these really needed?
    pub goal_x : u16,
    pub goal_y : u16,
}

pub struct Level {
    pub info : Vec<char>,
    map : GameMap,
    color_map : ColorMap,
    player : Player,
    pub next_level : String,
    pub max_cell_x : u16,
    pub max_cell_y : u16,
    color_map_src : ColorMap,
    map_src       : GameMap,
    message_who  : String,
    message_what : String,
    pub sfx_stack : Vec<(SoundTypes, i32)>,
}

impl Level {
    pub fn get_player(&self) -> &Player {
        &self.player
    }
    pub fn get_player_mut(&mut self) -> &mut Player {
        &mut self.player
    }

    pub fn get_map(&self) -> &GameMap {
        &self.map
    }

    pub fn get_colormap(&self) -> &ColorMap {
        &self.color_map
    }
    pub fn get_map_mut(&mut self) -> &mut GameMap {
        &mut self.map
    }
    pub fn get_colormap_mut(&mut self) -> &mut ColorMap {
        &mut self.color_map
    }

    pub fn get_tile(&mut self, y : usize, x : usize) -> char {
        self.map[y][x]
    }

    // don't actually do anything more than store.
    pub fn show_message(&mut self, who : &String, what : &String) {
        self.get_player_mut().base.stop();
        self.message_who.clone_from(who);
        self.message_what.clone_from(what);
    }

    // get the stored message or nothing
    pub fn get_message(&self) -> Option<(String, String)> {
        if self.message_who.is_empty() {
            return None
        } else {
            Some((self.message_who.clone(), self.message_what.clone()))
        }
    }
    pub fn prepare_frame(&mut self) {
        self.message_who.clear();
        self.message_what.clear();
        self.map.clone_from(&self.map_src);
        self.color_map.clone_from(&self.color_map_src);
    }

    pub fn get_map_src_mut(&mut self) -> &mut GameMap {
        &mut self.map_src
    }

    pub fn play_sound(&mut self, soundid : SoundTypes, volume : Option<i32>) {
        self.sfx_stack.push((soundid, volume.unwrap_or(sdl2::mixer::MAX_VOLUME)));
    }

    fn put_entity(&mut self, base : &BaseEntity) {
        let y = base.get_y_i32() as usize;
        let x = base.get_x_i32() as usize;
        self.put_char(y, x, base.sprite, base.color);
    }

   fn put_char(&mut self, y : usize, x : usize, sprite : char, color : Color) {
        self.map[y][x] = sprite;
        self.color_map[y][x] = color;
    }
    fn draw_string(&mut self, y : usize, x : usize, txt : &String) {
        let mut idx : usize = 0;
        txt.chars().for_each(|c| {
            self.put_char(y, x + idx, c, 1);
            idx = idx + 1;
        });
    }
    fn draw_frame(&mut self, y : usize, x :usize, height : usize, width : usize) {
        self.draw_string(y, x, &format!(".{}.", "-".repeat(width)));
        let line = format!("|{}|", " ".repeat(width));
        for i in 0..height {
            self.draw_string(y + 1 + i, x, &line);
        }
        self.draw_string(y + height, x, &format!("`{}'", "-".repeat(width)));
    }
}

pub struct Scene<'a> {
    pub name : String,
    pub metadata : MetaData,
    pub starting_path : String,
    pub entity_factory : &'a EntityFactory,
    pub level : Level,
    pub entities : Entities,
    pub max_cell_x : u16,
    pub max_cell_y : u16,
}

fn convert_char_to_color(input : char) -> Color {
    return input as Color - '0' as Color;
}

impl<'a> Scene<'a> {
    pub fn new(gamepath : &str, ef : &'a EntityFactory, max_x : u16, max_y : u16) -> Self {
        Self {
            name : String::default(),
            metadata : MetaData::default(),
            starting_path : String::from(gamepath),
            entity_factory : ef,
            level : Level::default(),
            entities : Entities::default(),
            max_cell_y : max_y,
            max_cell_x : max_x,
        }
    }

    pub fn start_frame(&mut self) {
        self.level.prepare_frame();
    }


    pub fn end_frame(&mut self) {
        let mut iter = self.entities.iter();
        while let Some(entity) = iter.next() {
            if !entity.get_base().hidden {
                match entity.get_base().width {
                    1 => {
                        self.level.put_entity(entity.get_base());
                    },
                    _ => {
                        let y = entity.get_base().get_y_i32() as usize;
                        let x = entity.get_base().get_x_i32() as usize;
                        let s = entity.get_base().sprite;
                        let c = entity.get_base().color;
                        let width = entity.get_base().width;
                        for j in 0..width as usize {
                            self.level.put_char(y, x + j, s, c);
                        }
                    }
                }
            }
        }
        self.level.put_char(self.get_player().base.get_y_i32() as usize,
                            self.get_player().base.get_x_i32() as usize,
                            self.get_player().base.sprite,
                            self.get_player().base.color);

        let msg = self.level.get_message();
        if msg.is_some() {
            let (who, what) = msg.unwrap();
            let max_y = self.level.max_cell_y as f32;
            let max_x = self.level.max_cell_x as f32;
            let text_width = f32::min(max_x * 0.8, 56.0).floor() as usize;

            let mut mx = 4;
            if text_width > 40 { mx = 2 }

            let wrapped = format!("{}", textwrap::fill(&what.replace("\\", "\n"), text_width as usize));
            let text_height = f32::min(max_y * 0.8, wrapped.lines().count() as f32 + 1.0).floor() as usize;

            let offset_x = ((max_x - text_width as f32 - mx as f32) / 2.0) as usize;
            let mut offset_y = ((max_y - text_height as f32) / 2.0 - 1.0) as usize;

            self.level.draw_frame(offset_y - 1, offset_x - mx, text_height + 2, text_width + mx * 2);
            self.level.draw_string(offset_y - 1, offset_x, &format!("[ {} says: ]", who));
            offset_y += 1;
            wrapped.split("\n").for_each::<_>(|line| {
                self.level.draw_string(offset_y, offset_x + 1, &format!("{}", line));
                offset_y += 1;
            });

            self.level.draw_string(offset_y + 1, offset_x + text_width - 15 + mx, &"[ (A) / Space ]".to_string());
        }
    }

    pub fn step(&mut self, delta : f32, player_input : &Vec<input::Input>) -> Vec<PFReturnType> {
        let mut retvalues = Vec::new();
        let mut iter = self.entities.iter_mut();
        for event in player_input.into_iter() {
            match event {
                input::Input::Nothing => {},
                input::Input::LEFT => {},
                input::Input::RIGHT => {},
                input::Input::UP => {},
                input::Input::DOWN => {},
                input::Input::ACTION => {},
                input::Input::QUIT => {
                    return vec![PFReturnType::QuitGame];
                },
                input::Input::COLOR => {
                    retvalues.push(PFReturnType::ToggleColor);
                },
                input::Input::MUSIC => {
                    retvalues.push(PFReturnType::ToggleMusic);
                },
                input::Input::RESTART => {
                    self.level.next_level = self.name.clone();
                    return vec![PFReturnType::TheLevelChanged];
                }
                _ => {}
            }
        }
        while let Some(entity) = iter.next() {
            let retval = entity.process_delta(delta, &mut self.level);
            match retval {
                PFReturnType::NothingToReport => {},
                PFReturnType::QuitGame => {
                    return vec![retval];
                },
                PFReturnType::TheLevelChanged => {
                    return vec![retval];
                },
                PFReturnType::ToggleMusic | PFReturnType::ToggleColor => {
                     retvalues.push(retval);
                }
                PFReturnType::PauseTiming => {
                    return vec![retval];
                },
            }
        }

        // some player stuffs
        let level = &mut self.level;
        let old_state = level.get_player_mut().base.get_state();
        let new_state = level.get_player_mut().process_input(player_input);
        if old_state != new_state && new_state == EntityState::Airborne {
            level.play_sound(SoundTypes::JUMP, None);
        }
        process_physics(&mut self.level.player.base, delta, &mut self.level.map);
        let old_state = new_state;
        let new_state = self.level.player.base.get_state();
        if old_state != new_state && new_state == EntityState::Dead {
            self.level.player.kill();
            self.level.play_sound(SoundTypes::DEATH, None);
        }
        retvalues
    }

    pub fn get_player_mut(&mut self) -> &mut Player {
        &mut self.level.player
    }
    pub fn get_player(&self) -> &Player {
        &self.level.player
    }

    pub fn switch_level(&mut self, level : &str) {
        Self::load_level(self, level);
        Self::get_player_mut(self).stop();
    }

    fn load_level(&mut self, name : &str) {
        self.name = String::from(name);
        self.level = Level::default();
        self.level.max_cell_x = self.max_cell_x;
        self.level.max_cell_y = self.max_cell_y;

        let base_path = Path::new(self.starting_path.as_str()).join("data");
        let map_path = base_path.clone().join(name).join("map.txt");
        let colormap_path = base_path.clone().join(name).join("colormap.txt");
        let metadata_path = base_path.clone().join(name).join("metadata.txt");

        self.entities.clear();
        Self::read_metadata(self, &metadata_path);
        Self::load_map(self, &map_path);
        Self::load_colormap(self, &colormap_path);
        let mut iter = self.entities.iter_mut();
        while let Some(entity) = iter.next() {
            let y = entity.get_base().get_y();
            entity.get_mut_base().set_y(y);
        }
    }

    fn parse_metaline(&mut self, command : &str, iter: &mut SplitAsciiWhitespace) {
        match command {
            "title" => {
                self.metadata.title.clear();
                while let Some(val) = iter.next() {
                    self.metadata.title += " ";
                    self.metadata.title += val;
                }
                let mut info : Vec<char> = vec![];
                self.metadata.title.chars().for_each(|c| info.push(c));
                self.level.info = info;
            },
            "size" => {
                let (width, height) = read_x_y(iter, 0);
                self.metadata.width = width;
                self.metadata.height = height;
            },
            "start" => {
                let (x, y) = read_x_y::<f32>(iter, 0.0);
                self.level.get_player_mut().base.set_x(x);
                self.level.get_player_mut().base.set_y(y);
            }
            _ => {
                match self.entity_factory.create_entity(command, iter) {
                    Some(entity) => {
                        self.entities.push(entity);
                    },
                    _ => {}
                }
            }
        }
    }

    fn read_metadata(&mut self, file_path : &PathBuf) -> bool {
        if let Ok(file) = File::open(file_path) {
            let mut reader = BufReader::new(file);
            let mut line = String::new();
            while let Ok(len) = reader.read_line(&mut line) {
                if len == 0 {
                    return true;
                }
                let mut iter = line.split_ascii_whitespace();
                match iter.next() {
                    Some(command) => Self::parse_metaline(self, command, &mut iter),
                    _ => ()
                };
                line.clear();
            }
            return true
        }
        false
    }

    fn load_map(&mut self, file_path : &PathBuf) {
        if let Ok(file) = File::open(file_path) {
            let mut reader = BufReader::new(file);
            let mut line = String::new();

            while let Ok(len) = reader.read_line(&mut line) {
                if len == 0 {
                    break;
                }
                let mut row : Vec<char> = line.chars().collect();
                let padding = self.metadata.width as i16 - row.len() as i16;
                if padding > 0 {
                    let mut pads = [' ' as char].repeat(padding as usize);
                    row.append(&mut pads);
                }
                self.level.map_src.push(row);
                line.clear();
            }
        }
    }


    fn load_colormap(&mut self, file_path : &PathBuf) {
        if let Ok(file) = File::open(file_path) {
            let mut reader = BufReader::new(file);
            let mut line = String::new();
            while let Ok(len) = reader.read_line(&mut line) {
                if len == 0 {
                    break;
                }
                let row : Vec<Color> =
                    line.chars().take_while(|c| (*c > ' '))
                                .map(|c| convert_char_to_color(c))
                                .chain(repeat(0 as Color).take(self.metadata.width as usize - (len-1)))
                                .collect();

                self.level.color_map_src.push(row);
                line.clear();
            }
        }
    }
}


#[test]
fn test_math() {
    assert_eq!(convert_char_to_color('1'), 1);
}

impl Default for Level {
    fn default() -> Self {
        Self {
            info : vec![],
            map : GameMap::default(),
            color_map : ColorMap::default(),
            player : Default::default(),
            next_level : Default::default(),
            max_cell_y : 0,
            max_cell_x : 0,
            map_src : GameMap::default(),
            color_map_src : ColorMap::default(),
            message_who : String::default(),
            message_what : String::default(),
            sfx_stack : Vec::default(),
        }
    }
}
