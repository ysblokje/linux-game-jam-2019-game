use std::path::Path;

use crate::entity::entity::EntityFactory;
use crate::entity;

use crate::{io::{audio::SoundTypes, input::Input}, level::Scene};

extern crate clap;

pub struct GameConfig<'a> {
    pub toggle_music : Box<dyn Fn() + 'a>,
    pub toggle_color : Box<dyn Fn() + 'a>,
    pub fetch_input : Box<dyn Fn() -> Vec<Input> + 'a>,
    pub play_sound : Box<dyn Fn(&SoundTypes, i32) + 'a>,
    pub play_music : Box<dyn Fn(&Path) + 'a>,
    pub render : Box<dyn Fn(&Scene) + 'a>,
}


pub fn game_loop(config : &GameConfig) {
    let music_file = "./ascii-platformer/data/audio/midnight-tale-by-kevin-macleod.ogg";

    let mut starting_level = "menu";
    let mut starting_folder = "ascii-platformer";
    let matches = clap_app!(cursed_platformer =>
                            (version: "0.1")
                            (author: "Samsai, Tuubi, Ysblokje")
                            (@arg BASEGAME: -g --game +takes_value "Sets custom game folder")
                            (@arg MAP: -m --map +takes_value "Starts the game at the specified level")
    ).get_matches();

    if let Some(alt_starting_folder) = matches.value_of("BASEGAME") {
        starting_folder = alt_starting_folder;
    }
    if let Some(alt_starting_level) = matches.value_of("MAP") {
        starting_level = alt_starting_level;
    }
    let mut entity_factory = EntityFactory::create_factory();
    entity::register_assemblyline(&mut entity_factory);
    let mut scene = Scene::new(starting_folder, &entity_factory, 80, 25);

    (config.play_music)(Path::new(music_file));

    scene.switch_level(starting_level);

    let mut quit : bool = false;
    let mut skip_render = false;
    let mut pause = false;

    let frame_length = std::time::Duration::from_secs_f32(1.0 / 60.0);
    let mut last = std::time::Instant::now() - frame_length;
    let mut time_delta : std::time::Duration;

    while !quit {
        time_delta = last.elapsed();
        last = std::time::Instant::now();
        scene.start_frame();
        let key = &(config.fetch_input)();
        let step_retval = scene.step(time_delta.as_secs_f32(), &key);

        for retval in step_retval.into_iter() {
            match retval {
                entity::entity::PFReturnType::NothingToReport => {
                }
                entity::entity::PFReturnType::TheLevelChanged => {
                    let old_level = scene.name.to_string();
                    let next_level = scene.level.next_level.clone();
                    scene.switch_level(next_level.as_str());
                    skip_render = true;
                    if old_level != next_level {
                        scene.level.play_sound(SoundTypes::GOAL, None)
                    }
                    break;
                }
                entity::entity::PFReturnType::PauseTiming => {
                    pause = true;
                }
                entity::entity::PFReturnType::ToggleMusic => {
                    (config.toggle_music)();
                }
                entity::entity::PFReturnType::ToggleColor => {
                    (config.toggle_color)();
                }

                entity::entity::PFReturnType::QuitGame => {
                    quit = true;
                }
            }
        }

        for sfx in scene.level.sfx_stack.pop() {
            (config.play_sound)(&sfx.0, sfx.1);
        }


        if !skip_render {
            scene.end_frame();
            (config.render)(&scene);
        } else {
            skip_render = false;
        }

        if pause {
            loop {
                let events = (config.fetch_input)();
                if events.contains(&Input::ACTION) {
                    pause = false;
                    break;
                }
                std::thread::sleep(frame_length);
                last = std::time::Instant::now();
            }
        } else if time_delta < frame_length {
            std::thread::sleep(frame_length - time_delta);
        }
    }

}
