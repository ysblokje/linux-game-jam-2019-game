extern crate sdl2;

use std::borrow::Borrow;

use sdl2::{event::Event, keyboard::Keycode, pixels::Color, render::{Canvas, Texture},  video::Window};

static font_path : &str = r#"ascii-platformer/data/Px437_AmstradPC1512-2y.ttf"#;

struct SDL2Stuff {
    sdl_context : Option<sdl2::Sdl>,
    window : Option<sdl2::video::Window>,
    canvas : Option<Canvas<Window>>,
    font_h : u32,
    font_w : u32,
}


pub fn sdl2_test() {
    let mut state = SDL2Stuff {
        sdl_context : None,
        window : None,
        canvas : None,
        font_h : 16,
        font_w : 16,
    };
    state.sdl_context = Some(sdl2::init().unwrap());
    let ttf_context = sdl2::ttf::init().unwrap();

    // Load a font
    let mut font = ttf_context.load_font(font_path, 16).unwrap();
//    font.set_style(sdl2::ttf::FontStyle::BOLD);
    font.set_kerning(true);
    font.set_hinting(sdl2::ttf::Hinting::Mono);
    let (font_w, font_h) = font.size_of_char('W').unwrap();

    state.font_w = font_w;
    state.font_h = font_h;

    let screen_width : u32 = 80 * state.font_w;
    let screen_height : u32 = 25 * state.font_h;

    // 7 or 8 is all we need
    let mut colors : Vec<Color> = Vec::new();
    colors.push(Color::RGBA(0,0,0, 255));
    colors.push(Color::RGBA(255,255,255, 255));
    colors.push(Color::RGBA(255,0,0, 255));
    colors.push(Color::RGBA(0,255,0, 255));
    colors.push(Color::RGBA(0,0,255, 255));
    colors.push(Color::RGBA(0,255,255, 255));
    colors.push(Color::RGBA(255,0,255, 255));
    colors.push(Color::RGBA(255,255,0, 255));

    state.window = match state.sdl_context.borrow().as_ref().unwrap().video().unwrap()
        .window("SDL2_TTF Example", screen_width as u32, screen_height as u32)
        .position_centered()
        .build() {
            Ok(window) => Some(window),
            Err(_) => return
        };

    let canvas = match state.window {
        Some(w) =>  match w.into_canvas().accelerated().present_vsync().build()  {
            Ok(c) => { c }
            Err(_) => { return }
        }
        None => { return }
    };

    let texture_creator = canvas.texture_creator();
    let mut glyph_set : Vec<Vec<Texture>> = Vec::new();

    for color in colors.iter() {
        let mut glyphs : Vec<Texture> = Vec::new();
        for c in 32 as u8 ..=127 as u8 {
            let mut txt : String = String::new();
            txt.push(c as char);

            let surface = font
                .render_char(c as char)
                .blended(*color)
                .unwrap();
            let texture = match texture_creator.create_texture_from_surface(surface) {
                Ok(a) => a,
                _ => unreachable!(),
            };
            glyphs.push(texture);
        }
        glyph_set.push(glyphs);
    }

    state.canvas = Some(canvas);
    //               0        1         2         3        4         5          6         7         8
    //          012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
    let text = "11111111112222222222333333333344444444445555555555666666666677777777778888888888";

    // everything should be set up at this point.


    let start_x = 0;
    let start_y = 0;

    let src_rect = sdl2::rect::Rect::new(0,0, state.font_w, state.font_h);
    let mut dest_rect = sdl2::rect::Rect::new(start_x*state.font_w as i32,start_y*state.font_w as i32, state.font_w,state.font_h);

    if let Some(mut canvas) = state.canvas {
        canvas.set_draw_color(colors[0]);
        canvas.clear();
        let mut idx : usize = 1;
        for i in 0..25 {
            dest_rect.set_y(i * state.font_h as i32);
            dest_rect.set_x(0);
            for c in text.chars() {
                canvas.copy(&glyph_set[idx][c as usize - 32], src_rect, dest_rect);
                dest_rect.set_x(dest_rect.x()+state.font_w as i32);
            }
            idx += 1;
            if idx > 7 {
                idx = 1;
            }
        }
        canvas.present();
    }

//    std::thread::sleep(std::time::Duration::from_secs(10));
    'mainloop: loop {
        let mut event_pump = match state.sdl_context.borrow() {
            Some(sdl) => {
                match sdl.event_pump() {
                    Ok(p) => { p }
                    Err(_) => {
                        return
                    }
                }
            }
            None => { return }
        };
        for event in event_pump.poll_iter() {
            match event {
                Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } | Event::Quit { .. } => break 'mainloop,
                _ => {}
            }
        }
    }

}
