#[derive(PartialEq)]
pub enum Input {
    Nothing,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    ACTION,
    QUIT,
    COLOR,
    MUSIC,
    RESTART,
    STOPMOVINGLEFT,
    STOPMOVINGRIGHT
}
