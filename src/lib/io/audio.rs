use std::path::Path;

pub enum SoundTypes {
    DEATH,
    GOAL,
    JUMP,
    PRESSUREPLATE,
    RUBBLE,
    TRAMPOLINE,
    TELEPORT,
}

pub trait AudioTraits {
    fn init_audio(&mut self) -> Result<bool, &str>;
    fn play_sound(&mut self, sound : &SoundTypes, volume : i32);
    fn play_music(&mut self, music_file : &Path);
    fn toggle_music(&mut self);
}
