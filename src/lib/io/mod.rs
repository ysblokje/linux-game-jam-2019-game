pub struct IOCapabilities {
    has_audio_interface : bool,
    has_input_interface : bool,
    has_render_interface : bool,
}

impl IOCapabilities {
    fn new() -> IOCapabilities {
        IOCapabilities {
            has_audio_interface : false,
            has_input_interface : false,
            has_render_interface: false
        }
    }
}

pub trait IO {
    fn get_capabilities() -> IOCapabilities;
}

pub mod input;
pub mod renderer;
pub mod audio;
pub mod curses;
pub mod our_sdl2;
