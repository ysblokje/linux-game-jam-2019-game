extern crate pancurses;
use std::path::Path;

use crate::{entity::entity::{Color}, level::Scene};
use crate::io::input::Input;
use super::{IO, IOCapabilities, audio::{AudioTraits, SoundTypes}, renderer::RenderTraits};
use pancurses::*;

pub struct CursesIO {
    width : i32,
    height: i32,
    use_color : bool,
    mainwin : Option<pancurses::Window>,
}

impl IO for CursesIO {
    fn get_capabilities() -> IOCapabilities {
        let mut retval  = IOCapabilities::new();
        retval.has_audio_interface = true;
        retval.has_input_interface = true;
        retval.has_render_interface = true;
        retval
    }
}

impl AudioTraits for CursesIO {
    fn init_audio(&mut self) -> Result<bool, &str>{
        Ok(true)
    }
    fn play_sound(&mut self, _sound : &SoundTypes, _ : i32) {
        pancurses::beep();
    }
    fn play_music(&mut self, _music_file : &Path) {}
    fn toggle_music(&mut self) {}
}

impl RenderTraits for CursesIO {

    fn render(&mut self, scene : &Scene) {
        if let Some(mainwin) = &self.mainwin {
            let mut x_offset = std::cmp::min(scene.get_player().base.get_x_i32() - (self.width / 2),
                                             scene.metadata.width as i32 - self.width);
            x_offset = std::cmp::max(x_offset, 0);
            let mut y_offset = std::cmp::min(scene.get_player().base.get_y_i32() - (self.height / 2),
                                             scene.metadata.height as i32 - self.height);
            y_offset = std::cmp::max(y_offset, 0);

            let x_max = std::cmp::min(self.width, scene.metadata.width as i32);
            let y_max = std::cmp::min(self.height, scene.metadata.height as i32);
            let mut color : Color = 0;


            let color_map = scene.level.get_colormap();
            let map = scene.level.get_map();

            if scene.level.info.len() > 0 {
                let mut x = 0;
                mainwin.attron(COLOR_PAIR(1 as chtype));
                for c in scene.level.info.iter() {
                    mainwin.mvaddch(0, x, *c as chtype);
                    x = x + 1;
                }
            }

            for y in 0..y_max {
                for x in 0..x_max {
                    let x_index = (x + x_offset) as usize;
                    let y_index = (y + y_offset) as usize;
                    if self.use_color {
                        if color_map[y_index][x_index] != color {
                            color = color_map[y_index][x_index];
                            if color != 0 {
                                mainwin.attron(COLOR_PAIR(color as chtype));
                            } else {
                                mainwin.attroff(COLOR_PAIR(color as chtype));
                            }
                        }
                    }
                    mainwin.mvaddch(y + 1, x, map[y_index][x_index]);
                }
            }
            mainwin.refresh();
        }
    }
    fn toggle_color(&mut self) {
        self.use_color = !self.use_color;
    }
}

impl CursesIO {
    pub fn new() -> CursesIO {
        CursesIO{
            width : 0,
            height: 0,
            use_color : false,
            mainwin : None,
        }
    }
    pub fn init_renderer(&mut self, max_width : u32, max_height : u32) -> Result<bool, &str>{
        pancurses::initscr();
        pancurses::noecho();
        pancurses::curs_set(0);

        if has_colors() {
            start_color();
            init_pair(1, COLOR_WHITE, COLOR_BLACK);
            init_pair(2, COLOR_RED, COLOR_BLACK);
            init_pair(3, COLOR_GREEN, COLOR_BLACK);
            init_pair(4, COLOR_BLUE, COLOR_BLACK);
            init_pair(5, COLOR_CYAN, COLOR_BLACK);
            init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
            init_pair(7, COLOR_YELLOW, COLOR_BLACK);
        }

        self.width = max_width as i32;
        self.height = max_height as i32;
        self.mainwin = Some(newwin(max_height as i32, max_width as i32, 0, 0));
        self.use_color = has_colors();

        if let Some(mainwin) = &self.mainwin {
            mainwin.nodelay(true);
            mainwin.keypad(true);
            mainwin.erase();
        } else {
                return Err("Unable to create Renderer : mainwin is faulty");
        }

        Ok(true)
    }
    pub fn init_input(&mut self) -> Result<bool, &str>{
        if self.mainwin.is_none() {
            return Err("Initialize output first");
        }
        Ok(true)
    }

    pub fn read(&mut self) -> Vec<Input> {
        let mut retval : Vec<Input> = Vec::new();
        if let Some(mainwin) = &self.mainwin {
            if let Some(key) = mainwin.getch() {
                match key {
                    pancurses::Input::Character(c) => {
                        match c {
                            'm' => retval.push(Input::MUSIC),
                            'q' => retval.push(Input::QUIT),
                            'c' => retval.push(Input::COLOR),
                            ' ' => retval.push(Input::ACTION),
                            'r' => retval.push(Input::RESTART),
                            _ => ()
                        }
                    }
                    pancurses::Input::KeyDown => {
                        retval.push(Input::DOWN);
                    }
                    pancurses::Input::KeyUp => {
                        retval.push(Input::UP);
                    }
                    pancurses::Input::KeyLeft => {
                        retval.push(Input::LEFT);
                    }
                    pancurses::Input::KeyRight => {
                        retval.push(Input::RIGHT);
                    }
                    _ => ()
                }
            }
        }
        retval
    }
}
// Just restore the output to a sane state.
impl Drop for CursesIO {
    fn drop(&mut self) {
        pancurses::endwin();
    }
}
