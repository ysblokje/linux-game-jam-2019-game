extern crate sdl2;
use sdl2::{controller::{Button, GameController}, event::Event, keyboard::Keycode, mixer::{AUDIO_S16LSB, DEFAULT_CHANNELS, InitFlag, Music, Sdl2MixerContext, Chunk}, render::{Texture, WindowCanvas}};
use crate::entity::entity;

use super::{IO, IOCapabilities, audio::{AudioTraits, SoundTypes}, input::Input};
use std::collections::HashMap;
use std::path::Path;


pub struct SDL2 {
    game_controller_subsys : Option<sdl2::GameControllerSubsystem>,
    mixer_context : Option<Sdl2MixerContext>,
    music : Option<Music<'static>>,
    initialized : bool,
    sfx_list : HashMap<String, Chunk>,
    controller : Option<GameController>,
    width : i32,
    height : i32,
    use_color : bool,
}

impl IO for SDL2 {
    fn get_capabilities() -> IOCapabilities {
        let mut retval  = IOCapabilities::new();
        retval.has_audio_interface = true;
        retval.has_input_interface = true;
        retval.has_render_interface = true;
        retval
    }
}


impl AudioTraits for SDL2 {
    fn init_audio(&mut self) -> Result<bool, &str> {
        match sdl2::mixer::open_audio(44100, AUDIO_S16LSB, DEFAULT_CHANNELS, 1024) {
            Ok(_) => {
                if let Ok(ctx) = sdl2::mixer::init(InitFlag::OGG) {
                    self.mixer_context = Some(ctx);
                    self.initialized = true;
                }
            },
            Err(_) => {
                return Err("Unable to initialize sdl audio");
            }
        }
        sdl2::mixer::allocate_channels(16);

        // Load the sound effects
        let chunk = Chunk::from_file(Path::new("./ascii-platformer/data/audio/death.ogg")).unwrap();
        let _ = &self.sfx_list.insert("death".to_string(), chunk);
        let chunk = Chunk::from_file(Path::new("./ascii-platformer/data/audio/goal.ogg")).unwrap();
        let _ = &self.sfx_list.insert("goal".to_string(), chunk);
        let chunk = Chunk::from_file(Path::new("./ascii-platformer/data/audio/jump.ogg")).unwrap();
        let _ = &self.sfx_list.insert("jump".to_string(), chunk);
        let chunk = Chunk::from_file(Path::new("./ascii-platformer/data/audio/pressureplate.ogg")).unwrap();
        let _ = &self.sfx_list.insert("pressureplate".to_string(), chunk);
        let chunk = Chunk::from_file(Path::new("./ascii-platformer/data/audio/rubble.ogg")).unwrap();
        let _ = &self.sfx_list.insert("rubble".to_string(), chunk);
        let chunk = Chunk::from_file(Path::new("./ascii-platformer/data/audio/sproing.ogg")).unwrap();
        let _ = &self.sfx_list.insert("trampoline".to_string(), chunk);

        Ok(true)
    }

    fn play_sound(&mut self, sound : &SoundTypes, volume : i32) {
        if self.initialized {
            let vol = volume;
            sdl2::mixer::Channel::all().set_volume(vol);
            fn play_chunk(sfx : &Chunk) {
                sdl2::mixer::Channel::all().play(sfx, 0).unwrap();
            }
            let sfx_list = &self.sfx_list;
            match sound {
                SoundTypes::DEATH => {
                    play_chunk(sfx_list.get("death").unwrap());
                },
                SoundTypes::GOAL => {
                    play_chunk(sfx_list.get("goal").unwrap());
                },
                SoundTypes::JUMP => {
                    play_chunk(sfx_list.get("jump").unwrap());
                },
                SoundTypes::PRESSUREPLATE => {
                    play_chunk(sfx_list.get("pressureplate").unwrap());
                },
                SoundTypes::RUBBLE => {
                    play_chunk(sfx_list.get("rubble").unwrap());
                },
                SoundTypes::TRAMPOLINE => {
                    play_chunk(sfx_list.get("trampoline").unwrap());
                }
                _ => { }
            }
        }
    }

    fn play_music(&mut self, music_file : &Path) {
        if self.initialized {
            let music = sdl2::mixer::Music::from_file(music_file).unwrap();
            music.play(-1).unwrap();
            self.music = Some(music);
        }
    }

    fn toggle_music(&mut self) {
        if self.initialized {
            if sdl2::mixer::Music::is_playing() {
                sdl2::mixer::Music::fade_out(1000).unwrap();
            } else {
                let music = self.music.as_ref().unwrap();
                music.fade_in(-1, 1000).unwrap();
            }
        }
    }
}

/*
 implement some type of renderer here
 */


impl SDL2 {
    pub fn new() -> SDL2 {

        SDL2 {
            game_controller_subsys: None,
            mixer_context : None,
            music : None,
            initialized : false,
            sfx_list : HashMap::new(),
            controller: None,
            width : 80,
            height : 25,
            use_color : true
        }
    }
    pub fn init_input(&mut self, sdl_context : &mut sdl2::Sdl) -> Result<bool, &str> {
            if self.game_controller_subsys.is_none() {
                self.game_controller_subsys = match sdl_context.game_controller() {
                    Ok(gcs) => {
                        Some(gcs)
                    },
                    Err(_) => {
                        return Err("Can't create SDL controller subsystem");
                    }
                };
            }

            let gcs = &self.game_controller_subsys.as_mut().unwrap();

            let available = match gcs.num_joysticks() {
                Ok(nr) => { nr }
                Err(_) => {
                    return Err("Unable to enumerate controllers");
                }
            };

            let controller = (0..available)
                .find_map( | id | {
                    if !gcs.is_game_controller(id) {
                        return None;
                    }
                    match gcs.open(id) {
                        Ok(c) => {
                            Some(c)
                        }
                        Err(_) => {
                            None
                        }
                    }
                });

            match controller {
                Some(c) => {
                    self.controller = Some(c);
                }
                None => {}
            }

            return Ok(true);
    }

    pub fn read(&mut self, event_pump : &mut sdl2::EventPump) -> Vec<Input> {
        let mut retval :  Vec<Input> = Vec::new();
                event_pump.poll_iter().for_each(|event| {
                    match event {
                        Event::ControllerButtonUp { button, .. } => {
                            match button {
                                Button::DPadLeft => {
                                    retval.push(Input::STOPMOVINGLEFT);
                                }
                                Button::DPadRight => {
                                    retval.push(Input::STOPMOVINGRIGHT);
                                }
                                _ => {},
                            }
                        }
                        Event::KeyUp { keycode: Some(keycode), repeat: false, .. }  => {
                            match keycode {
                                Keycode::Left => {
                                    retval.push(Input::STOPMOVINGLEFT);
                                },
                                Keycode::Right => {
                                    retval.push(Input::STOPMOVINGRIGHT);
                                },
                                _ => {},
                            }
                        }
                        Event::KeyDown { keycode: Some(keycode), repeat: false, .. }  => {
                            match keycode {
                                Keycode::Space => {
                                    retval.push(Input::ACTION);
                                },
                                Keycode::Left => {
                                    retval.push(Input::LEFT);
                                }
                                Keycode::Right => {
                                    retval.push(Input::RIGHT);
                                }
                                Keycode::Up => {
                                    retval.push(Input::UP);
                                }
                                Keycode::Down => {
                                    retval.push(Input::DOWN);
                                }
                                Keycode::M => {
                                    retval.push(Input::MUSIC);
                                }
                                Keycode::C => {
                                    retval.push(Input::COLOR);
                                }
                                Keycode::Q => {
                                    retval.push(Input::QUIT);
                                }
                                Keycode::R => {
                                    retval.push(Input::RESTART);
                                }
                                _ => {},
                            }
                        }
//                        Event::KeyUp { timestamp, window_id, keycode, scancode, keymod, repeat } => {}
                        Event::ControllerButtonDown { button, .. } => {
                            match button {
                                Button::A => {
                                    retval.push(Input::ACTION);
                                }
                                Button::B => {}
                                Button::X => {}
                                Button::Y => {}
                                Button::Back => {
                                    retval.push(Input::RESTART);
                                }
                                Button::Guide => {}
                                Button::Start => {
                                    retval.push(Input::QUIT);
                                }
                                Button::LeftStick => {}
                                Button::RightStick => {}
                                Button::LeftShoulder => {}
                                Button::RightShoulder => {}
                                Button::DPadUp => {
                                    retval.push(Input::UP);
                                }
                                Button::DPadDown => {
                                    retval.push(Input::DOWN);
                                }
                                Button::DPadLeft => {
                                    retval.push(Input::LEFT);
                                }
                                Button::DPadRight => {
                                    retval.push(Input::RIGHT);
                                }
                            }

                        }
                        _ => {}
                    }
                });
        retval
    }
    pub fn render(&mut self, scene : &crate::level::Scene, canvas : &mut WindowCanvas,
                  glyph_set : &Vec<Vec<Texture>>, font_h : u32, font_w : u32) {
        canvas.clear();

        let mut x_offset = std::cmp::min(scene.get_player().base.get_x_i32() - (self.width / 2),
                                         scene.metadata.width as i32 - self.width);
        x_offset = std::cmp::max(x_offset, 0);
        let mut y_offset = std::cmp::min(scene.get_player().base.get_y_i32() - (self.height / 2),
                                         scene.metadata.height as i32 - self.height);
        y_offset = std::cmp::max(y_offset, 0);

        let x_max = std::cmp::min(self.width, scene.metadata.width as i32);
        let y_max = std::cmp::min(self.height, scene.metadata.height as i32);
        let mut color : entity::Color = 0;


        let color_map = scene.level.get_colormap();
        let map = scene.level.get_map();

//        let mut glyphs = &glyph_set[color as usize];
        let src_rect = sdl2::rect::Rect::new(0,0, font_w, font_h);
        let mut dest_rect = sdl2::rect::Rect::new(0,0, font_w, font_h);

        let mut draw_char = |ch : char, color : usize, y : i32, x : i32| {
            if color == 0 {
                return
            }
            dest_rect.x = x;
            dest_rect.y = y;
            let glyphs = &glyph_set[color];
            if let Some(err) = canvas.copy(&glyphs[ch as usize - 32], src_rect, dest_rect).err() {
                println!("error : {}", err);
            }
        };

        if scene.level.info.len() > 0 {
            let mut x = 0;
            for c in scene.level.info.iter() {
                draw_char(*c, 1, 0, x);
                x = x + font_w as i32;
            }
        }

        let mut y_dest = font_h as i32;


        for y in 0..y_max {
            let mut x_dest = 0;
            for x in 0..x_max {
                let x_index = (x + x_offset) as usize;
                let y_index = (y + y_offset) as usize;
                if self.use_color {
                    if color_map[y_index][x_index] != color {
                        color = color_map[y_index][x_index];
                    }
                } else {
                    color = 1;
                }
                draw_char(map[y_index][x_index], color as usize, y_dest, x_dest);
                x_dest = x_dest + font_w as i32;
            }
            y_dest = y_dest + font_h as i32;
        }
        canvas.present();
    }
    pub fn toggle_color(&mut self) {
        self.use_color = !self.use_color;
    }
}


impl Drop for SDL2 {
    fn drop(&mut self) {
        self.music = None;
    }
}
