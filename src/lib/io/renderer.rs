
use crate::level::Scene;

pub trait RenderTraits {
//    fn init_renderer(&mut self, width : u32, height : u32) -> Result<bool, &str>;
    fn render(&mut self, scene : &Scene);
    fn toggle_color(&mut self);
}
