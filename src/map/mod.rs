use crate::entity::entity::Color;

pub type GameMap = Vec<Vec<char>>;
pub type ColorMap = Vec<Vec<Color>>;
