extern crate pancurses;
use crate::{entity::entity::{BaseEntity, Color}, level::Level, lib::renderer::RenderTraits};
use pancurses::*;

pub struct CursesRenderer {
    width : i32,
    height: i32,
    infowin : pancurses::Window,
    mainwin : pancurses::Window,
    use_color : bool,
}

impl RenderTraits for CursesRenderer {
    fn render(&mut self, level : &Level) {
        let mut x_offset = std::cmp::min(level.player.base.x as i32 - (self.width / 2),
                                         level.metadata.width as i32 - self.width);
        x_offset = std::cmp::max(x_offset, 0);
        let mut y_offset = std::cmp::min(level.player.base.y as i32 - (self.height / 2),
                                         level.metadata.height as i32 - self.height);
        y_offset = std::cmp::max(y_offset, 0);

        let x_max = std::cmp::min(self.width, level.metadata.width as i32);
        let y_max = std::cmp::min(self.height, level.metadata.height as i32);
        let mut color : Color = 0;

        self.infowin.mvaddstr(0, 0, "LEVEL ");
        self.infowin.mvaddnstr(0, 6, &level.metadata.title, 70);

        for y in 0..y_max {
            for x in 0..x_max {
                let x_index = (x + x_offset) as usize;
                let y_index = (y + y_offset) as usize;
                if self.use_color {
                    if level.color_map[y_index][x_index] != color {
                        if color != 0 {
                            self.mainwin.attron(COLOR_PAIR(color as chtype as chtype));
                        }
                        color = level.color_map[y_index][x_index];
                        if color != 0 {
                            self.mainwin.attroff(COLOR_PAIR(color as chtype as chtype));
                        }
                    }
                }
                self.mainwin.mvaddch(y, x, level.map[y_index][x_index]);
            }
        }

//        let mut iter = (&level.entities).into_iter();
        let draw_cell = |entity : &BaseEntity, j| {
            if entity.color != 0 {
                self.mainwin.attron(COLOR_PAIR(entity.color as chtype));
            }
            self.mainwin.mvaddch(entity.y as i32 - y_offset, entity.x as i32 + j - x_offset, entity.sprite);
            if entity.color != 0 {
                self.mainwin.attroff(COLOR_PAIR(entity.color as chtype));
            }
        };

        let mut iter = level.entities.iter();
        while let Some(entity) = iter.next() {
            let base = entity.get_base();
            if !base.disabled && !base.hidden {
                match base.width {
                    1 => {
                        draw_cell(base, 0);
                    },
                    _ => {
                        for j in 0..base.width {
                            draw_cell(base, j);
                        }

                    }
                }
            }
        }

        draw_cell(&level.player.base, 0);

        self.mainwin.refresh();
        self.infowin.refresh();
        self.mainwin.getch();
    }
}

impl CursesRenderer {
    pub fn new(max_width : i32, max_height : i32) -> CursesRenderer {
        pancurses::initscr();
        if has_colors() {
            start_color();
            init_pair(1, COLOR_WHITE, COLOR_BLACK);
            init_pair(2, COLOR_RED, COLOR_BLACK);
            init_pair(3, COLOR_GREEN, COLOR_BLACK);
            init_pair(4, COLOR_BLUE, COLOR_BLACK);
            init_pair(5, COLOR_CYAN, COLOR_BLACK);
            init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
            init_pair(7, COLOR_YELLOW, COLOR_BLACK);
        }

        let retval = CursesRenderer {
            width : max_width,
            height : max_height,
            infowin : newwin(1, max_width, 0, 0),
            mainwin : newwin(max_height - 1, max_width, 1, 0),
            use_color : has_colors()
        };
        retval.infowin.erase();
        retval.mainwin.erase();
        return retval;
    }

    pub fn end(&mut self) {
        endwin();
    }
}
