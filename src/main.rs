use level::Level;
use lib::renderer::RenderTraits;
use renderer::pancurses_renderer::*;

pub mod lib;
pub mod entity;
pub mod map;
pub mod level;
pub mod audio;
pub mod renderer;

fn main() {
    let mut entity_factory = entity::entity::EntityFactory::create_factory();
    entity::register_assemblyline(&mut entity_factory);
    let mut level = Level::new("ascii-platformer", &entity_factory);
    level.switch_level("menu");

    let mut renderer = CursesRenderer::new(80, 25);
    // while etc.etc.
    renderer.render(&level);
    // done
    renderer.end();
}
